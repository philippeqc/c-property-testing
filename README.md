# Prop Test

This library provides a property testing framework for C++20, as well as several fake data generators (e.g. names, credit
card info, etc.). The fake data generators integrate with the property testing framework, that way developers can test
their code with realistic data as well as with randomized data.

Additionally, the property testing framework may be used in conjunction with another test framework (e.g. [doctest](https://github.com/doctest/doctest,
[Catch2](https://github.com/catchorg/Catch2), [GTest](https://github.com/google/googletest)), or it may be used with the
built-in test framework, or even without any framework.

The library is set up to be included through CMake's FetchContent. It also may be downloaded and added as a CMake
subdirectory. By adding it as a subdirectory, the library will grab and download its dependencies.

```cmake
include(FetchContent)
FETCHCONTENT_DECLARE(
        prop_test
        GIT_REPOSITORY https://gitlab.com/mtolman1/prop-test-framework.git
        GIT_TAG master
)
FETCHCONTENT_MAKEAVAILABLE(prop_test)
```

## CMake Options

* `PROP_FRAMEWORK_BUILD_TESTS`
  * Determines whether to build tests
* `PROP_FRAMEWORK_BUILD_DOCS`
  * Determines whether to build documentation
* `PROP_FRAMEWORK_BUILD_EXAMPLES`
  * Determines whether to build documentation

## Environment Variables (Runtime)

These environment variables are available regardless of which usage method is used.

* `PROP_TEST_SEED`
  * Determines the seed to use for random value generation
* `PROP_TEST_DATA_VERSION`
  * Determines the default data version for random value generation
* `PROP_TEST_ITERS`
  * Determines the default number of iterations to use for property checks

## Usage

Usage may be done in either without any test framework, with the built-in framework, or with an external framework.
In all cases, link the library `mtolman::prop_test`.

If your framework isn't listed here, you can still use Prop Test with your test framework. You'll just need to use the
"No Framework" example and wrap it in a test case for your test framework of choice. You'll then need to (at minimum)
have an assert on the `prop_test::Suite`'s `did_pass` method. Though it is recommended to print out the information from
the failures and 

### No Framework

To do property testing without a framework, you'll need to create a `prop_test::Suite` and then run tests with that suite.
You'll then need to check the results at the end and print them out (if desired).

```cpp
#include <prop_test.h>
#include <iostream>

// Helper methods to print our results
void print_failure(const prop_test::FailedCase& failure);
void print_summary(const prop_test::Suite& suite);

int main() {
  auto suite = prop_test::Suite("Example");

  // Define our test cases

  suite.with_generators(
         "Add Example - No Macros",
         prop_test::gen::integer<int>(),
         prop_test::gen::integer<int>())
    .run([](int a, int b) {
      if (a + b != b + a) {
        throw prop_test::AssertFailedError("Commutative property doesn't hold!");
      }
    });

  suite.with_generators(
         "Add Example - Assertion Macros",
         prop_test::gen::integer<int>(),
         prop_test::gen::integer<int>())
    .run([](int a, int b) {
      PROP_TEST_ASSERT(a + b == b + a);
    });

  // Process our output
  print_summary(suite);

  // We get a program exit code we can use
  return suite.program_exit_code();
}

// You may adapt these for your needs (e.g. print in a format that your CI system can parse)
void print_summary(const prop_test::Suite& suite) {
  const auto& summary = suite.result_summary();
  for (const auto& failed : summary.failedCases) {
    print_failure(failed.second);
  }
  for (const auto& errored : summary.erroredCases) {
    print_failure(errored.second);
  }
  std::cout
    << "\n\nPassed: " << summary.passedCases.size()
    << "\nFailed: " << summary.failedCases.size()
    << "\nErrored: " << summary.erroredCases.size() << "\n";
}

void print_failure(const prop_test::FailedCase& failure) {
  std::cout << (failure.error ? "Error: " : "Failed: ")
            << failure.testName
            << "\n"
            << failure.failureMessage
            << "\nShrunk Input: " << failure.simplifiedInputStr
            << "\n\n";
}
```

### Built-in framework

```cpp
// Will define a main function and allows self-registering
#define PROP_TEST_DEFINE_MAIN

// Header for built-in mode
#include <prop_test/tests.h>

// Defines a test suite (can be filtered with the --test-suites option)
// Test suites may be defined in different cpp files
PROP_TEST_SUITE("addition") {
  PROP_TEST_CASE(
    "Commutative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b) {
    PROP_TEST_ASSERT((a + b) == (b + a));
  };

  PROP_TEST_CASE(
    "Associative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b, int32_t c) {
    PROP_TEST_ASSERT((a + b) + c == a + (b + c));
  };

  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a + 0 == a);
  };
};
```

### Doctest

For doctest, link the library `mtolman::prop_test-doctest` in CMake. Then include the `doctest.h` header
*before* including the `#include <prop_test/doctest.h>` (this allows you to put doctest anywhere in your include path,
rename the file, etc.). For defining a main function, define `DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN` as you would with
doctest.

For creating a property test suite inside a doctest codebase, use the macros `PROP_SUITE` and `PROP_TEST_CASE` instead
of `TEST_SUITE` and `TEST_CASE`. Also, inside your property tests, use the macro `PROP_TEST_ASSERT` in order for
shrinking to happen properly (if you use `CHECK` then shrinking won't happen when there is a failure).

Example:

```cpp
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <prop_test/doctest.h>

PROP_SUITE("Addition") {
  PROP_TEST_CASE(
    "Commutative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b) {
    PROP_TEST_ASSERT((a + b) == (b + a));
  };

  PROP_TEST_CASE(
    "Associative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b, int32_t c) {
    PROP_TEST_ASSERT((a + b) + c == a + (b + c));
  };

  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a + 0 == a);
  };
}
```

### Catch2 (Version 3)

For Catch2, link the library `mtolman::prop_test-catch2` in CMake (you will still need to link the Catch2 library as well).
Then include the Catch2 headers *before* including the `#include <prop_test/catch2.h>`. For defining a main function, 
just follow the Catch2 documentation.

For creating a property test suite inside a doctest codebase, use the macros `PROP_SUITE` and `PROP_TEST_CASE` instead
of `TEST_SUITE` and `TEST_CASE`. Also, inside your property tests, use the macro `PROP_TEST_ASSERT` in order for
shrinking to happen properly (if you use `CHECK` then shrinking won't happen when there is a failure).

Example:

```cpp
#include <catch2/catch_test_macros.hpp>
#include <prop_test/catch2.hpp>

PROP_SUITE("Addition", "[addition]") {
  PROP_TEST_CASE(
    "Commutative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b) {
    PROP_TEST_ASSERT((a + b) == (b + a));
  };

  PROP_TEST_CASE(
    "Associative Property",
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>(),
    prop_test::gen::integer<int32_t>())
  (int32_t a, int32_t b, int32_t c) {
    PROP_TEST_ASSERT((a + b) + c == a + (b + c));
  };

  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a + 0 == a);
  };
}
```

## Dependencies

The Prop Test library depends on the following:
* boost::pfr
  * This is a standalone Boost library which doesn't need the rest of Boost
  * Used for creating objects from multiple generators
* magic_enum
  * Standalone header library
  * Used for providing enum generators
* docopt
  * for the built-in framework mode, used for CLI argument parsing
* faker-data
  * This holds the data set for fake data generation
* doctest (Testing; doctest integration)
  * Used for testing the property testing framework. Also used for the doctest integration
* Catch2 (catch2 integration)
  * Used for the Catch2 integration
* Doxygen (documentation)
  * Used to build docs

## CMake Library Targets

* `mtolman::prop_tests`
  * Library to link to for using the Prop Tests library (both no-framework and framework versions)
* `mtolman::prop_tests-doctest`
  * Library to link to for using the Prop Tests library's Doctest integration
* `mtolman::prop_tests-catch2`
  * Library to link to for using the Prop Tests library's Catch2 (Version 3) integration
  * Must still link to Catch2 separately
