#include <doctest/doctest.h>
#include <prop_test.h>

TEST_SUITE("People") {
  TEST_CASE("Person") {
    auto gen = prop_test::gen::faker::people::person();
    for (size_t i = 0; i < 1000; ++i) {
      auto p = gen.create(i);
      CHECK_GT(p.surName.size(), 0);
      CHECK_GT(p.givenName.size(), 0);
      CHECK_NE(p.fullName.find(p.surName), std::string::npos);
      CHECK_NE(p.fullName.find(p.givenName), std::string::npos);
    }
  }
}
