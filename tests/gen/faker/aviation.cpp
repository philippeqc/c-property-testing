#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>

TEST_SUITE("Aviation") {
  TEST_CASE("Aircraft Types") {
    const auto gen = prop_test::gen::faker::aviation::aircraft_types_generator();
    const auto regex = std::regex(R"(^\w+$)");
    for (size_t i = 0; i < 100; ++i) {
      CHECK(std::regex_match(gen.create(i), regex));
    }
  }

  const auto nameRegex = std::regex(R"(^[\w\s\-\/\(\)\d\.\']+$)");
  const auto iataRegex = std::regex(R"(^[\w\d]{2,3}$)");
  const auto icaoRegex = std::regex(R"(^[\w\d]{3,5}$)");

  TEST_CASE("Airlines") {
    const auto gen = prop_test::gen::faker::aviation::airlines_generator();
    for (size_t i = 0; i < 1000; ++i) {
      const auto v = gen.create(i);
      CHECK_MESSAGE(std::regex_match(v.name, nameRegex), (v.name));
      CHECK_MESSAGE(std::regex_match(v.iata, iataRegex), (v.iata));
      CHECK_MESSAGE(std::regex_match(v.icao, icaoRegex), (v.icao));
    }
  }

  TEST_CASE("Airports") {
    const auto gen = prop_test::gen::faker::aviation::airports_generator();
    for (size_t i = 0; i < 1000; ++i) {
      const auto v = gen.create(i);
      CHECK_MESSAGE(std::regex_match(v.name, nameRegex), (v.name));
      CHECK_MESSAGE(std::regex_match(v.iata, iataRegex), (v.iata));
      CHECK_MESSAGE(std::regex_match(v.icao, icaoRegex), (v.icao));
    }
  }

  TEST_CASE("Airplanes") {
    const auto gen = prop_test::gen::faker::aviation::airplanes_generator();
    for (size_t i = 0; i < 1000; ++i) {
      const auto v = gen.create(i);
      CHECK_MESSAGE(std::regex_match(v.name, nameRegex), (v.name));
      CHECK_MESSAGE(std::regex_match(v.iata, iataRegex), (v.iata));
      CHECK_MESSAGE(std::regex_match(v.icao, icaoRegex), (v.icao));
    }
  }
}
