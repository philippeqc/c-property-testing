#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>
#include <iostream>

TEST_SUITE("Internet") {
  TEST_CASE("Top Level Domains") {
    auto gen = prop_test::gen::faker::internet::top_level_domain();
    for (size_t i = 0; i < 1000; ++i) {
      auto p = gen.create(i);
      CHECK_GT(p.size(), 0);
    }
  }

  TEST_CASE("Protocols") {
    auto gen = prop_test::gen::faker::internet::url_schemas();
    for (size_t i = 0; i < 1000; ++i) {
      auto p = gen.create(i);
      CHECK_GT(p.size(), 0);
    }
  }

  TEST_CASE("IP V4") {
    auto gen = prop_test::gen::faker::internet::ip_v4();
    auto regex = std::regex(R"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})");
    for (size_t i = 0; i < 1000; ++i) {
      CHECK_MESSAGE(std::regex_match(gen.create(i), regex), gen.create(i));
    }
  }

  TEST_CASE("IP V6") {
    auto gen = prop_test::gen::faker::internet::ip_v6();
    auto regex = std::regex(R"((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))");
    for (size_t i = 0; i < 1000; ++i) {
      CHECK_MESSAGE(std::regex_match(gen.create(i), regex), gen.create(i));
    }
  }

  TEST_CASE("Domain") {
    auto gen = prop_test::gen::faker::internet::domain();
    auto regex = std::regex(R"([a-z\-\.0-9A-Z]+)");
    for (size_t i = 0; i < 1000; ++i) {
      CHECK_MESSAGE(std::regex_match(gen.create(i), regex), gen.create(i));
    }
  }

  TEST_CASE("Url") {
    const auto fullUrlRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noIpV4Regex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|([a-z\-0-9A-Z]+\.)+[a-z\-A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noIpV6Regex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noEncodedRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+])+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+])+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+])+)?(\#([a-zA-Z0-9\-\._&\?=\/\+])+)?)");
    const auto noHashRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noPathRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?(\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noUsernameRegex = std::regex(R"((https?|s?ftps?):\/\/((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noPasswordRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noQueryRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto noPortRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");

    const auto ipv4Regex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto ipv6Regex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\]))(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");
    const auto domainRegex = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?(([a-z\-0-9A-Z]+\.)+[a-z\-A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})+)?)");

    const auto ipv4NoFlagsRegex = std::regex(R"((https?|s?ftps?):\/\/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\/?)");
    const auto ipv6NoFlagsRegex = std::regex(R"((https?|s?ftps?):\/\/(\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])\/?)");
    const auto domainNoFlagsRegex = std::regex(R"((https?|s?ftps?):\/\/(([a-z\-0-9A-Z]+\.)+[a-z\-A-Z]+)\/?)");

    const auto expectedSimplifiedRegex = std::vector<std::regex>{
      // No IPV4
      noIpV4Regex,
      // No IPV6
      noIpV6Regex,
      // No Encoded Chars
      noEncodedRegex,
      // No Hash
      noHashRegex,
      // No Path
      noPathRegex,
      // No Username
      noUsernameRegex,
      // No Password
      noPasswordRegex,
      // No Query
      noQueryRegex,
      //No Port
      noPortRegex
    };

    SUBCASE("Generate") {
      auto gen = prop_test::gen::faker::internet::url();
      for (size_t i = 0; i < 100; ++i) {
        CHECK_MESSAGE(std::regex_match(gen.create(i), fullUrlRegex), gen.create(i));
      }
    }

    SUBCASE("Shrink") {
      using namespace prop_test::gen::faker::internet;
      auto gen = url({}, prop_test::gen::faker::DataVersion::VERSION_0);
      SUBCASE("IPV4") {
        const auto index = 15;
        const auto generatedUrl = gen.create(index);
        const auto noFlagsUrl = url({.flags=UrlOptions::ALLOW_IPV4_HOST}, prop_test::gen::faker::DataVersion::VERSION_0).create(index);
        CHECK_MESSAGE(std::regex_match(generatedUrl, ipv4Regex), generatedUrl);
        CHECK_MESSAGE(std::regex_match(noFlagsUrl, ipv4NoFlagsRegex), noFlagsUrl);

        const auto simplified = gen.simplify_index(index).branches();
        REQUIRE_EQ(simplified.size(), expectedSimplifiedRegex.size());
        for (size_t i = 0; i < simplified.size(); ++i) {
          CHECK_MESSAGE(std::regex_match(simplified[i].value, expectedSimplifiedRegex[i]), simplified[i].value);
        }
      }

      SUBCASE("IPV6") {
        const auto index = 88;
        const auto generatedUrl = gen.create(index);
        CHECK_MESSAGE(std::regex_match(generatedUrl, ipv6Regex), generatedUrl);
        const auto noFlagsUrl = url({.flags=UrlOptions::ALLOW_IPV6_HOST}, prop_test::gen::faker::DataVersion::VERSION_0).create(index);
        CHECK_MESSAGE(std::regex_match(noFlagsUrl, ipv6NoFlagsRegex), noFlagsUrl);

        const auto simplified = gen.simplify_index(index).branches();
        REQUIRE_EQ(simplified.size(), expectedSimplifiedRegex.size());
        for (size_t i = 0; i < simplified.size(); ++i) {
          CHECK_MESSAGE(std::regex_match(simplified[i].value, expectedSimplifiedRegex[i]), simplified[i].value);
        }
      }

      SUBCASE("Domain") {
        const auto index = 556;
        const auto generatedUrl = gen.create(index);
        CHECK_MESSAGE(std::regex_match(generatedUrl, domainRegex), generatedUrl);
        const auto noFlagsUrl = url({.flags=0x0}, prop_test::gen::faker::DataVersion::VERSION_0).create(index);
        CHECK_MESSAGE(std::regex_match(noFlagsUrl, domainNoFlagsRegex), noFlagsUrl);

        const auto simplified = gen.simplify_index(index).branches();
        REQUIRE_EQ(simplified.size(), expectedSimplifiedRegex.size());
        for (size_t i = 0; i < simplified.size(); ++i) {
          CHECK_MESSAGE(std::regex_match(simplified[i].value, expectedSimplifiedRegex[i]), simplified[i].value);
        }
      }
    }
  }
}
