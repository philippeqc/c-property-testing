#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>


TEST_SUITE("Colors") {

  TEST_CASE("RGB") {
    auto generator = prop_test::gen::faker::colors::rgb_generator();
    auto hexRegex = std::regex("\\#[A-F0-9]{6}");
    auto cssRegex = std::regex(R"(rgb\((\s*\d+\s*,){2}\s*\d+\s*\))");
    for (size_t i = 0; i < 1000; ++i) {
      auto rgb = generator.create(i);
      CHECK_GE(rgb.red, 0);
      CHECK_LE(rgb.red, 255);

      CHECK_GE(rgb.green, 0);
      CHECK_LE(rgb.green, 255);

      CHECK_GE(rgb.blue, 0);
      CHECK_LE(rgb.blue, 255);

      CHECK_MESSAGE(std::regex_match(rgb.hex(), hexRegex), rgb.hex());
      CHECK_MESSAGE(std::regex_match(rgb.css(), cssRegex), rgb.css());
    }
  }

  TEST_CASE("RGBA") {
    auto generator = prop_test::gen::faker::colors::rgba_generator();
    auto hexRegex = std::regex("\\#[A-F0-9]{8}");
    auto cssRegex = std::regex(R"(rgba\((\s*\d+\s*,){3}\s*\d+\s*\))");
    for (size_t i = 0; i < 1000; ++i) {
      auto rgba = generator.create(i);
      CHECK_GE(rgba.red, 0);
      CHECK_LE(rgba.red, 255);

      CHECK_GE(rgba.green, 0);
      CHECK_LE(rgba.green, 255);

      CHECK_GE(rgba.blue, 0);
      CHECK_LE(rgba.blue, 255);

      CHECK_GE(rgba.alpha, 0);
      CHECK_LE(rgba.alpha, 255);

      CHECK_MESSAGE(std::regex_match(rgba.hex(), hexRegex), rgba.hex());
      CHECK_MESSAGE(std::regex_match(rgba.css(), cssRegex), rgba.css());
    }
  }

  TEST_CASE("HSL") {
    auto generator = prop_test::gen::faker::colors::hsl_generator();
    auto cssRegex = std::regex(R"(hsl\(\d{1,3}(\.\d+)?deg(,\s+\d{1,2}(\.\d+)?\%){2}\))");
    for (size_t i = 0; i < 1000; ++i) {
      auto hsl = generator.create(i);
      CHECK_GE(hsl.hue, 0);
      CHECK_LE(hsl.hue, 360);

      CHECK_GE(hsl.luminosity, 0);
      CHECK_LE(hsl.luminosity, 1);

      CHECK_GE(hsl.saturation, 0);
      CHECK_LE(hsl.saturation, 1);

      CHECK_MESSAGE(std::regex_match(hsl.css(), cssRegex), hsl.css());
    }
  }

  TEST_CASE("HSLA") {
    auto generator = prop_test::gen::faker::colors::hsla_generator();
    auto cssRegex = std::regex(R"(hsla\(\d{1,3}(\.\d+)?deg(,\s+\d{1,2}(\.\d+)?\%){2},\s+\d\.\d+\))");
    for (size_t i = 0; i < 1000; ++i) {
      auto hsla = generator.create(i);
      CHECK_GE(hsla.hue, 0);
      CHECK_LE(hsla.hue, 360);

      CHECK_GE(hsla.luminosity, 0);
      CHECK_LE(hsla.luminosity, 1);

      CHECK_GE(hsla.saturation, 0);
      CHECK_LE(hsla.saturation, 1);

      CHECK_GE(hsla.alpha, 0);
      CHECK_LE(hsla.alpha, 1);

      CHECK_MESSAGE(std::regex_match(hsla.css(), cssRegex), hsla.css());
    }
  }
}