#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>

#define LUHN_VALIDATION(card) \
    { \
        size_t sum = 0; \
        int multiplier = 2; \
        for (auto chIndex = (card).size() - 1; chIndex > 0; --chIndex) { \
            auto index = chIndex - 1; \
            sum += ((card)[index] - '0') * multiplier; \
            multiplier = multiplier == 1 ? 2 : 1; \
        } \
        const auto check = (9 - ((sum + 9) % 10)); \
        CHECK_EQ(check, ((card)[(card).size() - 1] - '0')); \
    }

TEST_SUITE("Finance") {
  TEST_CASE("Account Number") {
    const auto gen = prop_test::gen::faker::finance::account_number(12);
    const auto regex = std::regex(R"(^\d{12}$)");
    for (size_t i = 0; i < 100; ++i) {
      CHECK(std::regex_match(gen.create(i), regex));
    }
  }

  TEST_CASE("Account Name") {
    const auto gen = prop_test::gen::faker::finance::account_name();
    for (size_t i = 0; i < 100; ++i) {
      const auto v = gen.create(i);
      CHECK_GT(v.size(), 0);
    }
  }

  TEST_CASE("Transaction Type") {
    const auto gen = prop_test::gen::faker::finance::transaction_type();
    for (size_t i = 0; i < 100; ++i) {
      CHECK_GT(gen.create(i).size(), 0);
    }
  }

  TEST_CASE("Bic") {
    SUBCASE("No Branch") {
      const auto gen = prop_test::gen::faker::finance::bic({.includeBranchCode = false});
      for (size_t i = 0; i < 100; ++i) {
        CHECK_EQ(gen.create(i).size(), 8);
      }
    }

    SUBCASE("Branch") {
      const auto gen = prop_test::gen::faker::finance::bic({.includeBranchCode = true});
      for (size_t i = 0; i < 100; ++i) {
        CHECK_EQ(gen.create(i).size(), 11);
      }
    }
  }

  TEST_CASE("Card") {
    const auto gen = prop_test::gen::faker::finance::credit_card();
    for (size_t i = 0; i < 1000; ++i) {
      auto res = gen.create(i);
      // Run a Luhn checksum validation to check that we're generating "realistic" cards
      LUHN_VALIDATION(res.number)

      // Make sure we got a valid CVV
      for (auto ch : res.cvv) {
        CHECK_GE(ch, '0');
        CHECK_LE(ch, '9');
      }

      auto month = std::stoi(res.expiration.substr(0, 2));
      CHECK_GE(month, 1);
      CHECK_LE(month, 12);

      auto year = std::stoi(res.expiration.substr(3));
      CHECK_GE(year, 1900);
    }
  }

  TEST_CASE("Masked Number") {
    const auto gen = prop_test::gen::faker::finance::masked_number({.unMaskedLength = 4, .maskedLength = 4, .maskedChar = '#', .surroundWithParens = false});
    auto start = std::string(4, '#');
    auto regex = std::regex(R"(\#{4}\d{4})");
    for (size_t i = 0; i < 100; ++i) {
      CHECK(std::regex_match(gen.create(i), regex));
    }
  }

  TEST_CASE("Pin") {
    const auto gen = prop_test::gen::faker::finance::pin(4);
    const auto regex = std::regex(R"(^\d{4}$)");
    for (size_t i = 0; i < 100; ++i) {
      CHECK(std::regex_match(gen.create(i), regex));
    }
  }

  TEST_CASE("Currency") {
    const auto gen = prop_test::gen::faker::finance::currency();
    for (size_t i = 0; i < 1000; ++i) {
      CHECK_MESSAGE(gen.create(i).code.size() == 3, gen.create(i).code);
    }
  }
}
