#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Bitset") {
  TEST_CASE("Generation") {
    auto gen = prop_test::gen::collections::bitset<45>();
    for (size_t i = 0; i < 100; ++i) {
      auto val = gen.create(i);
      CHECK_EQ(val.size(), 45);
    }
  }

  TEST_CASE("Shrinkage") {
    auto gen = prop_test::gen::collections::bitset<25>();
    auto pred = [](const auto& v) { return v.size() < 10; };
    const auto start = 9;
    REQUIRE_FALSE_MESSAGE(pred(gen.create(start)), gen.create(start).size());
    auto shrunk = prop_test::shrink(start, gen, pred);
    CHECK_EQ(shrunk.size(), 25);
  }
}
