#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Array") {
  TEST_CASE("Generation") {
    auto gen = prop_test::gen::collections::array<45>(
      prop_test::gen::integer::ints({.min = 0, .max = 100})
    );
    for (size_t i = 0; i < 100; ++i) {
      auto val = gen.create(i);
      for (const auto& e : val) {
        CHECK_GE(e, 0);
        CHECK_LE(e, 100);
      }
      CHECK_EQ(val.size(), 45);
    }
  }

  TEST_CASE("Shrinkage") {
    auto gen = prop_test::gen::collections::array<25>(
      prop_test::gen::integer::ints({.min = 0, .max = 100})
    );
    auto pred = [](const std::array<int, 25>& v) { return v.size() < 10; };
    const auto start = 9;
    REQUIRE_FALSE_MESSAGE(pred(gen.create(start)), gen.create(start).size());
    auto shrunk = prop_test::shrink(start, gen, pred);
    CHECK_EQ(shrunk.size(), 25);
  }
}
