#include <prop_test.h>
#include <doctest/doctest.h>
#include <regex>

TEST_SUITE("ASCII GEN") {
  TEST_CASE("Can create ascii text") {
    SUBCASE("Printable") {
      SUBCASE("len 5") {
        auto generator = prop_test::gen::strings::ascii::printable({5, 5});
        for (size_t i = 0; i < 100; ++i) {
          CHECK_EQ(generator.create(i).size(), 5);
        }
      }

      SUBCASE("len 3-5") {
        auto generator = prop_test::gen::strings::ascii::printable({5, 5});
        for (size_t i = 0; i < 100; ++i) {
          auto s = generator.create(i).size();
          CHECK_GE(s, 3);
          CHECK_LE(s, 5);
        }
      }
    }

    SUBCASE("Alpha") {
      auto generator = prop_test::gen::strings::ascii::alpha();
      auto regex = std::regex("[a-zA-Z]+");

      for (size_t i = 0; i < 100; ++i) {
        CHECK(std::regex_match(generator.create(i), regex));
      }
    }

    SUBCASE("Numeric") {
      auto generator = prop_test::gen::strings::ascii::numeric();
      auto regex = std::regex("\\d+");

      for (size_t i = 0; i < 100; ++i) {
        CHECK(std::regex_match(generator.create(i), regex));
      }
    }

    SUBCASE("Hex") {
      auto generator = prop_test::gen::strings::ascii::hex();
      auto regex = std::regex("[a-fA-F0-9]+");

      for (size_t i = 0; i < 100; ++i) {
        CHECK(std::regex_match(generator.create(i), regex));
      }
    }

    SUBCASE("Text") {
      auto generator = prop_test::gen::strings::ascii::text();

      CHECK_GE(generator.create(0).size(), 1);
    }

    SUBCASE("Math") {
      auto generator = prop_test::gen::strings::ascii::math();

      CHECK_GE(generator.create(0).size(), 1);
    }
  }
}
