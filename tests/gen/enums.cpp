#include <doctest/doctest.h>
#include <prop_test.h>
#include <set>

enum TestEnum1 {
  TEST_1_A,
  TEST_1_B,
  TEST_1_C,
  TEST_1_D
};

enum class TestEnum2 {
  A, B, C, D, E
};

enum TestEnum3 : uint16_t {
  TEST_3_A = 10,
  TEST_3_B = 14,
  TEST_3_C = 20,
  TEST_3_D = 12
};

enum class TestEnum4 : uint16_t {
  A = 1, B = 10, C = 100, D = 5, E = 50
};

TEST_SUITE("Enum values") {
  TEST_CASE("C enum seq") {
    SUBCASE("No exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum1>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_GE(v, TEST_1_A);
        CHECK_LE(v, TEST_1_D);
      }
    }

    SUBCASE("Exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum1>({TEST_1_B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_GE(v, TEST_1_A);
        CHECK_LE(v, TEST_1_D);
        CHECK_NE(v, TEST_1_B);
      }
    }
  }

  TEST_CASE("enum class seq") {
    SUBCASE("No exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum2>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_GE(v, TestEnum2::A);
        CHECK_LE(v, TestEnum2::E);
      }
    }

    SUBCASE("Exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum2>({TestEnum2::B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_GE(v, TestEnum2::A);
        CHECK_LE(v, TestEnum2::E);
        CHECK_NE(v, TestEnum2::B);
      }
    }
  }

  TEST_CASE("C enum specified") {
    SUBCASE("No exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum3>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE((v == TEST_3_A || v == TEST_3_B || v == TEST_3_C || v == TEST_3_D), v);
      }
    }

    SUBCASE("Exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum3>({TEST_3_C});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE((v == TEST_3_A || v == TEST_3_B || v == TEST_3_D), v);
      }
    }
  }

  TEST_CASE("enum class specified") {
    SUBCASE("No exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum4>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE((v == TestEnum4::A || v == TestEnum4::B || v == TestEnum4::C || v == TestEnum4::D || v == TestEnum4::E), v);
      }
    }

    SUBCASE("Exclude") {
      const auto gen = prop_test::gen::enums::enum_value<TestEnum4>({TestEnum4::D});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE((v == TestEnum4::A || v == TestEnum4::B || v == TestEnum4::C || v == TestEnum4::E), v);
      }
    }
  }
}

TEST_SUITE("Enum names") {
  TEST_CASE("C enum seq") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TEST_1_A", "TEST_1_B", "TEST_1_C", "TEST_1_D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum1>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TEST_1_A", "TEST_1_C", "TEST_1_D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum1>({TEST_1_B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }

  TEST_CASE("enum class seq") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"A", "B", "C", "D", "E"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum2>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"A", "B", "C", "D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum2>({TestEnum2::E});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }
  TEST_CASE("C enum specified") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TEST_3_A", "TEST_3_B", "TEST_3_C", "TEST_3_D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum3>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TEST_3_A", "TEST_3_C", "TEST_3_D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum3>({TEST_3_B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }

  TEST_CASE("enum class specified") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"A", "B", "C", "D", "E"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum4>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"A", "B", "C", "D"};
      const auto gen = prop_test::gen::enums::enum_name<TestEnum4>({TestEnum4::E});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }
}


TEST_SUITE("Typed Enum names") {
  TEST_CASE("C enum seq") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TestEnum1::TEST_1_A", "TestEnum1::TEST_1_B", "TestEnum1::TEST_1_C", "TestEnum1::TEST_1_D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum1>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TestEnum1::TEST_1_A", "TestEnum1::TEST_1_C", "TestEnum1::TEST_1_D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum1>({TEST_1_B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }

  TEST_CASE("enum class seq") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TestEnum2::A", "TestEnum2::B", "TestEnum2::C", "TestEnum2::D", "TestEnum2::E"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum2>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TestEnum2::A", "TestEnum2::B", "TestEnum2::C", "TestEnum2::D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum2>({TestEnum2::E});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }
  TEST_CASE("C enum specified") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TestEnum3::TEST_3_A", "TestEnum3::TEST_3_B", "TestEnum3::TEST_3_C", "TestEnum3::TEST_3_D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum3>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TestEnum3::TEST_3_A", "TestEnum3::TEST_3_C", "TestEnum3::TEST_3_D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum3>({TEST_3_B});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }

  TEST_CASE("enum class specified") {
    SUBCASE("No exclude") {
      const auto names = std::set<std::string>{"TestEnum4::A", "TestEnum4::B", "TestEnum4::C", "TestEnum4::D", "TestEnum4::E"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum4>();
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }

    SUBCASE("Exclude") {
      const auto names = std::set<std::string>{"TestEnum4::A", "TestEnum4::B", "TestEnum4::C", "TestEnum4::D"};
      const auto gen = prop_test::gen::enums::typed_enum_name<TestEnum4>({TestEnum4::E});
      for (size_t i = 0; i < 25; ++i) {
        const auto v = gen.create(i);
        CHECK_MESSAGE(names.contains(v), v);
      }
    }
  }
}
