#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("One Of Generator") {
  TEST_CASE("Same type") {
    auto gen = prop_test::gen::utils::one_of(
      prop_test::gen::integer::ints<int>({.min = 0, .max = 6}),
      prop_test::gen::integer::ints<int>({.min = 100, .max = 106})
    );

    SUBCASE("generate value") {
      for (size_t i = 0; i < 100; ++i) {
        const auto v = gen.create(i);
        CHECK_GE(v, 0);
        CHECK_LE(v, 106);
        if (v < 50) {
          CHECK_LE(v, 6);
        }
        else {
          CHECK_GE(v, 100);
        }
      }
    }

    SUBCASE("shrink") {
      auto predicate = [](auto i) { return i < 11; };
      auto start = 30;
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto simplified = prop_test::shrink(start, gen, predicate);
      // No simplification
      REQUIRE_EQ(simplified, gen.create(start));
    }
  }

  TEST_CASE("Different type") {
    auto gen = prop_test::gen::utils::one_of(
      prop_test::gen::integer::ints<int>({.min = 0, .max = 6}),
      prop_test::gen::fp::floats<float>({.min = 100, .max = 106})
    );

    SUBCASE("generate value") {
      for (size_t i = 0; i < 100; ++i) {
        const auto v = gen.create(i);
        if (std::holds_alternative<int>(v)) {
          const auto i = std::get<int>(v);
          CHECK_GE(i, 0);
          CHECK_LE(i, 6);
        }
        else {
          const auto f = std::get<float>(v);
          CHECK_GE(f, 100.0f);
          CHECK_LE(f, 106.0f);
        }
      }
    }

    SUBCASE("shrink") {
      auto predicate = [](auto i) { return false; };
      auto start = 40;
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto simplified = prop_test::shrink(start, gen, predicate);
      // No simplification
      REQUIRE_EQ(simplified, gen.create(start));
    }
  }
}
