#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Just") {
  TEST_CASE("Returns the same value") {
    auto gen = prop_test::gen::utils::just(5);

    SUBCASE("generate value") {
      for (size_t i = 0; i < 100; ++i) {
        CHECK_EQ(gen.create(i), 5);
      }
    }

    SUBCASE("shrink") {
      auto predicate = [](auto i) { return i < 2; };
      auto start = 40;
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto simplified = prop_test::shrink(start, gen, predicate);
      REQUIRE_EQ(simplified, 5);
    }
  }
}
