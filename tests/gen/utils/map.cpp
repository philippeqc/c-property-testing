#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Map Generator") {
  TEST_CASE("Even Numbers") {
    auto gen = prop_test::gen::utils::map<int>(
      [](auto i){ return i % 2 == 0 ? i : i + 1; },
      prop_test::gen::integer::ints<int>({.min = 0, .max = 60})
    );

    SUBCASE("generate value") {
      for (size_t i = 0; i < 100; ++i) {
        CHECK_EQ(gen.create(i) % 2, 0);
      }
    }

    SUBCASE("Doesn't shrink") {
      auto predicate = [](auto i) { return i < 11; };
      auto start = 40;
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto simplified = prop_test::shrink(start, gen, predicate);
      REQUIRE_EQ(simplified, gen.create(start));
    }
  }
}
