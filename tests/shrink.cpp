#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>

TEST_SUITE("Shrink") {
  TEST_CASE("Simple integers") {
    auto gen = prop_test::gen::integer::ints<uint16_t>();
    auto start = 20;
    const auto predicate = [](uint16_t i) { return i < 10; };
    REQUIRE_FALSE(predicate(gen.create(start)));
    auto shrinkRes = prop_test::shrink(start, gen, predicate);
    REQUIRE_EQ(shrinkRes, 10);
  }

  TEST_CASE("Simple floats") {
    auto gen = prop_test::gen::fp::floats<double>({.min = 0, .max = 10000000});
    auto start = 999999999;
    const auto predicate = [](double i) { return i < 5; };
    REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
    auto shrinkRes = prop_test::shrink(start, gen, predicate);
    REQUIRE_GE(shrinkRes, 5);
    REQUIRE_LT(shrinkRes, gen.create(start));
  }

  TEST_CASE("ASCII") {
    auto gen = prop_test::gen::strings::ascii::hex({2, 12});
    auto start = 12;

    SUBCASE("first char") {
      const auto predicate = [](const std::string &h) { return h[0] == 'A'; };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_NE(shrinkRes[0], 'A');
    }

    SUBCASE("strlen") {
      const auto predicate = [](const std::string &h) { return h.size() == 2; };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_NE(shrinkRes.size(), 2);
    }

    SUBCASE("no number") {
      auto regex = std::regex("[^\\d]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }

    SUBCASE("no upper") {
      auto regex = std::regex("[^A-Z]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }

    SUBCASE("no lower") {
      auto regex = std::regex("[^a-z]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }
  }

  TEST_CASE("Extended ASCII") {
    auto gen = prop_test::gen::strings::eascii::hex({2, 12});
    auto start = 12;

    SUBCASE("first char") {
      const auto predicate = [](const std::string &h) { return h[0] == 'A'; };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_NE(shrinkRes[0], 'A');
    }


    SUBCASE("strlen") {
      const auto predicate = [](const std::string &h) { return h.size() == 2; };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_NE(shrinkRes.size(), 2);
    }

    SUBCASE("no number") {
      auto regex = std::regex("[^\\d]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }

    SUBCASE("no upper") {
      auto regex = std::regex("[^A-Z]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }

    SUBCASE("no lower") {
      auto regex = std::regex("[^a-z]+");
      const auto predicate = [&regex](const std::string &h) { return std::regex_match(h, regex); };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_FALSE(std::regex_match(shrinkRes,regex));
    }
  }

  TEST_CASE("Aviation") {
    SUBCASE("Aircraft Types") {
      auto gen = prop_test::gen::faker::aviation::aircraft_types_generator(prop_test::gen::faker::DataVersion::VERSION_0);
      auto start = 11;
      const auto predicate = [](const std::string &h) {
        return h == "regional";
      };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_EQ(shrinkRes, gen.create(start));
    }

    SUBCASE("Airlines") {
      auto gen = prop_test::gen::faker::aviation::airlines_generator(prop_test::gen::faker::DataVersion::VERSION_0);
      auto start = 11;
      const auto predicate = [](const auto &h) {
        return h.name.find('k') != std::string::npos;
      };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).name);
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_EQ(shrinkRes.name, gen.create(start).name);
    }

    SUBCASE("Airplanes") {
      auto gen = prop_test::gen::faker::aviation::airplanes_generator(prop_test::gen::faker::DataVersion::VERSION_0);
      auto start = 11;
      const auto predicate = [](const auto &h) {
        return h.name.find('k') != std::string::npos;
      };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).name);
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_EQ(shrinkRes.name, gen.create(start).name);
    }

    SUBCASE("Airports") {
      auto gen = prop_test::gen::faker::aviation::airports_generator(prop_test::gen::faker::DataVersion::VERSION_0);
      auto start = 11;
      const auto predicate = [](const auto &h) {
        return h.name.find('k') != std::string::npos;
      };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).name);
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_EQ(shrinkRes.name, gen.create(start).name);
    }
  }

  TEST_CASE("Colors") {
    SUBCASE("names") {
      auto gen = prop_test::gen::faker::colors::name_generator();
      auto start = 99999;
      const auto predicate = [](const std::string& color) { return color == "red"; };
      REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
      auto shrinkRes = prop_test::shrink(start, gen, predicate);
      CHECK_EQ(shrinkRes, gen.create(start));
    }

    SUBCASE("RGB") {
      auto gen = prop_test::gen::faker::colors::rgb_generator();

      SUBCASE("Shrink Red") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::RGB color) { return color.red < 30; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.red, 42);
      }

      SUBCASE("Shrink Green") {
        auto start = 29;
        const auto predicate = [](prop_test::gen::faker::colors::RGB color) { return color.green > 150; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).green);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.green, 150);
      }

      SUBCASE("Shrink Blue") {
        auto start = 250;
        const auto predicate = [](prop_test::gen::faker::colors::RGB color) { return color.blue < 32; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).blue);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_GE(shrinkRes.blue, 32);
        CHECK_LT(shrinkRes.blue, gen.create(start).blue);
      }

      SUBCASE("Shrink Hex") {
        auto start = 240;
        const auto predicate = [](prop_test::gen::faker::colors::RGB color) { return color.hex().find('5') != std::string::npos; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).hex());
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_EQ(shrinkRes.hex(), "#C62616");
      }
    }

    SUBCASE("RGBA") {
      auto gen = prop_test::gen::faker::colors::rgba_generator();

      SUBCASE("Shrink Red") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::RGBA color) { return color.red < 30; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start));
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_GE(shrinkRes.red, 30);
        CHECK_LT(shrinkRes.red, gen.create(start).red);
      }

      SUBCASE("Shrink Green") {
        auto start = 29;
        const auto predicate = [](prop_test::gen::faker::colors::RGBA color) { return color.green < 10; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).green);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_GE(shrinkRes.green, 10);
        CHECK_LT(shrinkRes.green, gen.create(start).green);
      }

      SUBCASE("Shrink Blue") {
        auto start = 250;
        const auto predicate = [](prop_test::gen::faker::colors::RGBA color) { return color.blue < 32; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).blue);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_GE(shrinkRes.blue, 32);
        CHECK_LT(shrinkRes.blue, gen.create(start).blue);
      }

      SUBCASE("Shrink Alpha") {
        auto start = 250;
        const auto predicate = [](prop_test::gen::faker::colors::RGBA color) { return color.alpha < 40; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).blue);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_GE(shrinkRes.alpha, 40);
        CHECK_LT(shrinkRes.alpha, gen.create(start).alpha);
      }

      SUBCASE("Shrink Hex") {
        auto start = 240;
        const auto predicate = [](prop_test::gen::faker::colors::RGBA color) { return color.hex().find('5') != std::string::npos; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).hex());
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_EQ(shrinkRes.hex(), "#C6261616");
      }
    }

    SUBCASE("HSL") {
      auto gen = prop_test::gen::faker::colors::hsl_generator();

      SUBCASE("Shrink hue") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::HSL color) { return color.hue < 45.0; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).hue);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.hue, 70);
        CHECK_GE(shrinkRes.hue, 45);
      }

      SUBCASE("Shrink luminosity") {
        auto start = 9999;
        const auto predicate = [](prop_test::gen::faker::colors::HSL color) { return color.luminosity < 0.450; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).luminosity);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.luminosity, 0.70);
        CHECK_GE(shrinkRes.luminosity, 0.45);
      }

      SUBCASE("Shrink saturation") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::HSL color) { return color.saturation < 0.450; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).saturation);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.saturation, 0.70);
        CHECK_GE(shrinkRes.saturation, 0.45);
      }
    }

    SUBCASE("HSLA") {
      auto gen = prop_test::gen::faker::colors::hsla_generator();

      SUBCASE("Shrink hue") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::HSLA color) { return color.hue < 45.0; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).hue);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.hue, 70);
        CHECK_GE(shrinkRes.hue, 45);
      }

      SUBCASE("Shrink luminosity") {
        auto start = 9999;
        const auto predicate = [](prop_test::gen::faker::colors::HSLA color) { return color.luminosity < 0.450; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).luminosity);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.luminosity, 0.70);
        CHECK_GE(shrinkRes.luminosity, 0.45);
      }

      SUBCASE("Shrink saturation") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::HSLA color) { return color.saturation < 0.450; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).saturation);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.saturation, 0.70);
        CHECK_GE(shrinkRes.saturation, 0.45);
      }

      SUBCASE("Shrink alpha") {
        auto start = 99999;
        const auto predicate = [](prop_test::gen::faker::colors::HSLA color) { return color.alpha < 0.450; };
        REQUIRE_FALSE_MESSAGE(predicate(gen.create(start)), gen.create(start).alpha);
        auto shrinkRes = prop_test::shrink(start, gen, predicate);
        CHECK_LE(shrinkRes.alpha, 0.70);
        CHECK_GE(shrinkRes.alpha, 0.45);
      }
    }
  }

  TEST_CASE("Finance") {
    SUBCASE("Amount") {
      auto generator = prop_test::gen::faker::finance::amount();
      auto start = 999999999;
      const auto predicate = [](const std::string& s) { return s.size() < 5; };
      REQUIRE_FALSE_MESSAGE(predicate(generator.create(start)), generator.create(start));
      auto shrinkRes = prop_test::shrink(start, generator, predicate);
      REQUIRE_GE(shrinkRes.size(), 5);
      REQUIRE_LT(shrinkRes.size(), generator.create(start).size());
    }
  }
}
