#pragma once

#include <functional>
#include <map>
#include <prop_test.h>
#include <prop_test/tests.h>
#include <sstream>

#define PROP_DOCTEST_ANONYMOUS(x) PROP_TEST_CAT(x, __LINE__)

namespace prop_test::doctest_integration {
  inline bool check_suite(const prop_test::Suite &suite) {
    const auto &summary = suite.result_summary();
    for (const auto &s : summary.failedCases) {
      std::stringstream ss;
      ss << s.second.failureMessage
         << "\nSeed: "
         << s.second.seed << "\nInput: " << s.second.inputStr << "\nSimplified: " << s.second.simplifiedInputStr;
      CHECK_MESSAGE(false, (ss.str()));
    }

    for (const auto &s : summary.erroredCases) {
      std::stringstream ss;
      ss << s.second.failureMessage
         << "\nSeed: "
         << s.second.seed << "\nInput: " << s.second.inputStr << "\nSimplified: " << s.second.simplifiedInputStr;
      auto fail = [str = ss.str()]() -> bool {
        throw std::runtime_error(str);
      };
      CHECK(fail());
    }
    return suite.did_pass();
  }
}// namespace prop_test::doctest_integration

#define PROP_SUITE_TRIALS(NAME, TRIALS)                                                     \
  static void PROP_DOCTEST_ANONYMOUS(PROP_TEST_ANNON_FN)(prop_test::Suite & qqqqqq_suites); \
  TEST_CASE(NAME) {                                                                         \
    auto suites = prop_test::Suite(NAME, TRIALS);                                           \
    PROP_DOCTEST_ANONYMOUS(PROP_TEST_ANNON_FN)                                              \
    (suites);                                                                               \
    prop_test::doctest_integration::check_suite(suites);                                    \
  }                                                                                         \
  void PROP_DOCTEST_ANONYMOUS(PROP_TEST_ANNON_FN)(prop_test::Suite & qqqqqq_suites)

#define PROP_SUITE(NAME) PROP_SUITE_TRIALS(NAME, prop_test::default_num_iters())
