#pragma once

#include <functional>
#include <map>
#include <prop_test.h>
#include <prop_test/tests.h>
#include <sstream>

#define PROP_CATCH2_ANONYMOUS(x) PROP_TEST_CAT(x, __LINE__)

namespace prop_test::catch2_integration {
  inline bool check_suite(const prop_test::Suite &suite) {
    const auto &summary = suite.result_summary();
    for (const auto &s : summary.failedCases) {
      std::stringstream ss;
      ss << s.second.failureMessage
         << "\nSeed: "
         << s.second.seed << "\nInput: " << s.second.inputStr << "\nSimplified: " << s.second.simplifiedInputStr;
      CHECK(ss.str() == "");
    }

    for (const auto &s : summary.erroredCases) {
      std::stringstream ss;
      ss << s.second.failureMessage
         << "\nSeed: "
         << s.second.seed << "\nInput: " << s.second.inputStr << "\nSimplified: " << s.second.simplifiedInputStr;
      auto fail = [str = ss.str()]() -> bool {
        throw std::runtime_error(str);
      };
      CHECK(fail());
    }
    return suite.did_pass();
  }
}// namespace prop_test::catch2_integration

#define PROP_SUITE_TRIALS(NAME, FLAGS, TRIALS)                                                     \
  static void PROP_CATCH2_ANONYMOUS(PROP_TEST_ANNON_FN)(prop_test::Suite & qqqqqq_suites); \
  TEST_CASE(NAME, FLAGS) {                                                                         \
    auto suites = prop_test::Suite(NAME, TRIALS);                                           \
    PROP_CATCH2_ANONYMOUS(PROP_TEST_ANNON_FN)                                              \
    (suites);                                                                               \
    prop_test::catch2_integration::check_suite(suites);                                    \
  }                                                                                         \
  void PROP_CATCH2_ANONYMOUS(PROP_TEST_ANNON_FN)(prop_test::Suite & qqqqqq_suites)

#define PROP_SUITE(NAME, FLAGS) PROP_SUITE_TRIALS(NAME, FLAGS, prop_test::default_num_iters())
