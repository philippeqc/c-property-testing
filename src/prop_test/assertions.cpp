#include <prop_test.h>
#include "prop_test/assertions.h"
#include <optional>

std::optional<size_t> globalSeed = std::nullopt;

auto prop_test::impl::get_seed() -> size_t {
  if (globalSeed) {
    return *globalSeed;
  }
  size_t seed;
  auto *seedCStr = getenv("PROP_TEST_SEED");
  if (seedCStr && seedCStr[0] != '\0') {
    seed = std::stoull(std::string{seedCStr});
  } else {
    std::random_device device;
    seed = (static_cast<uint64_t>(device()) << 32) | device();
  }
  return seed;
}

auto prop_test::impl::set_seed(size_t seed) -> void {
  globalSeed = seed;
}
