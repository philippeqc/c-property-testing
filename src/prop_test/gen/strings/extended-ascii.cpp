#include <prop_test/gen/strings/extended-ascii.h>
#include "ranges.h"
#include <unordered_map>
#include <random>

prop_test::gen::strings::eascii::impl::Generator::Generator(const prop_test::gen::strings::eascii::impl::Settings &settings)
    : settings(settings), cache({}), numChars(0) {
    init_cache();
}

static auto add_removed_chars(
  const std::string& str,
  const prop_test::gen::strings::eascii::impl::Settings& settings,
  std::vector<prop_test::Simplification<std::string, prop_test::gen::strings::eascii::impl::Simplifier>>& res
) {
    std::string buffer(str.size() - 1, '\0');
    for (size_t skip = 0; skip < str.size(); ++skip) {
      size_t bufferIndex = 0;
      for (size_t index = 0; index < str.size(); ++index) {
        if (index == skip) {
          continue;
        }
        buffer[bufferIndex++] = str[index];
      }
      res.emplace_back(prop_test::Simplification<std::string, prop_test::gen::strings::eascii::impl::Simplifier>{
        .value=buffer,
        .next={.settings=settings}
      });
    }
}


namespace prop_test::gen::strings::eascii::impl {
    struct EAsciiGroup {
      const char *name = "";
      std::bitset<256> value;
    };
}

static const auto eascii_groups = [](const prop_test::gen::strings::Options&options) {
  return std::array{
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"extended", prop_test::gen::strings::ranges::impl::eascii::extended(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"whitespace", prop_test::gen::strings::ranges::impl::eascii::whitespace(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"control", prop_test::gen::strings::ranges::impl::eascii::control(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alpha", prop_test::gen::strings::ranges::impl::eascii::alpha(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"numeric", prop_test::gen::strings::ranges::impl::eascii::numeric(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"octal", prop_test::gen::strings::ranges::impl::eascii::octal(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"binary", prop_test::gen::strings::ranges::impl::eascii::binary(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alphaNumeric", prop_test::gen::strings::ranges::impl::eascii::alphaNumeric(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"printable", prop_test::gen::strings::ranges::impl::eascii::printable(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"symbol", prop_test::gen::strings::ranges::impl::eascii::symbol(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"htmlNamed", prop_test::gen::strings::ranges::impl::eascii::htmlNamed(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"quote", prop_test::gen::strings::ranges::impl::eascii::quote(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"bracket", prop_test::gen::strings::ranges::impl::eascii::bracket(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"punctuation", prop_test::gen::strings::ranges::impl::eascii::sentencePunctuation(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"safe-punctuation", prop_test::gen::strings::ranges::impl::eascii::safePunctuation(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"text", prop_test::gen::strings::ranges::impl::eascii::text(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"math", prop_test::gen::strings::ranges::impl::eascii::math(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alpha-upper", prop_test::gen::strings::ranges::impl::eascii::alphaUpper(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alpha-lower", prop_test::gen::strings::ranges::impl::eascii::alphaLower(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alpha-numeric-upper", prop_test::gen::strings::ranges::impl::eascii::alphaNumericUpper(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"alpha-numeric-lower", prop_test::gen::strings::ranges::impl::eascii::alphaNumericLower(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"hex-upper", prop_test::gen::strings::ranges::impl::eascii::hexUpper(options.dataVersion)},
    prop_test::gen::strings::eascii::impl::EAsciiGroup{"hex-lower", prop_test::gen::strings::ranges::impl::eascii::hexLower(options.dataVersion)},
  };
};



static auto replace_char_groups(
  const std::string& str,
  const prop_test::gen::strings::eascii::impl::Settings& settings,
  std::vector<prop_test::Simplification<std::string, prop_test::gen::strings::eascii::impl::Simplifier>>& res
) {
    if (settings.allowedChars.none()) {
      return;
    }

    auto copy = str;
    for (const auto& group : eascii_groups(settings.options)) {
      auto combined = settings.allowedChars & group.value;
      if (combined.any()) {
        size_t first = 0;
        for (;first < combined.size(); ++first) {
          if (combined.test(first)) {
            break;
          }
        }
        auto replacementChar = static_cast<char>(first);
        copy = str;
        bool replaced = false;
        for (auto& ch : copy) {
          if (ch >= combined.size() || !combined.test(ch)) {
            ch = replacementChar;
            replaced = true;
          }
        }
        if (replaced) {
          res.emplace_back(prop_test::Simplification<std::string, prop_test::gen::strings::eascii::impl::Simplifier>{
            .value=copy,
            .next={.settings={.options={settings.options}, .allowedChars={}}}
          });
        }
      }
    }
}

auto prop_test::gen::strings::eascii::impl::Simplifier::branches(const std::string& str) const -> std::vector<Simplification<std::string, Simplifier>> {
    auto res = std::vector<Simplification<std::string, Simplifier>>{};
    res.reserve(str.size());

    if (str.size() > settings.options.minLen) {
      replace_char_groups(str, settings, res);
      add_removed_chars(str, settings, res);
    }

    res.shrink_to_fit();
    return res;
}

auto prop_test::gen::strings::eascii::impl::Generator::simplifier(const std::string& failed) const -> Simplification<std::string, Simplifier> {
    return {
      .value = failed,
      .next = Simplifier{.settings=settings}
    };
}

auto prop_test::gen::strings::eascii::impl::Generator::create(size_t index) const -> std::string {
    if (numChars == 0) {
      return "";
    }

    auto rnd = std::mt19937(index);
    // The formula helps us get some nice short inputs, long inputs, and normal-ish input lengths within the first 100 indices
    size_t len = settings.options.maxLen <= settings.options.minLen ? settings.options.minLen : (rnd() * numChars) % (settings.options.maxLen - settings.options.minLen) + settings.options.minLen;

    std::string result(len, '\0');
    if (result.empty()) {
      return result;
    }

    auto dst = std::uniform_int_distribution<size_t>(0, numChars - 1);

    for (char &c : result) {
      c = cache.at(dst(rnd));
    }
    return result;
}

void prop_test::gen::strings::eascii::impl::Generator::init_cache() {
    numChars = settings.allowedChars.count();
    for (size_t arrIndex = 0, bsIndex = 0; bsIndex < ranges::bits::num_bits<Range>; ++bsIndex) {
      if (settings.allowedChars[bsIndex]) {
        cache[arrIndex++] = static_cast<uint8_t>(bsIndex);
      }
    }
}

auto prop_test::gen::strings::eascii::any(Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::any(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alpha(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alphaLower(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alphaUpper(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::binary(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::binary(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::domain_piece(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::domainPiece(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::text(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::text(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::math(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::math(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha_numeric(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alphaNumeric(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha_numeric_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alphaNumericUpper(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::alpha_numeric_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::alphaNumericLower(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::symbol(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::symbol(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::html_named(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::htmlNamed(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::quote(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::quote(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::bracket(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::bracket(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::sentence_punctuation(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::sentencePunctuation(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::safe_punctuation(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::safePunctuation(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::hex(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::hex(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::hex_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::hexUpper(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::hex_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::hexLower(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::numeric(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::numeric(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::printable(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::printable(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::url_unencoded_chars(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::urlUnencodedChars(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::url_unencoded_hash_chars(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::urlUnencodedHashChars(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::extended(prop_test::gen::strings::Options options) -> impl::Generator{
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::extended(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::whitespace(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::whitespace(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::control(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::control(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::octal(prop_test::gen::strings::Options options) -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({
      .options=options,
      .allowedChars = prop_test::gen::strings::ranges::impl::eascii::octal(options.dataVersion)
    });
}
auto prop_test::gen::strings::eascii::empty() -> prop_test::gen::strings::eascii::impl::Generator {
    return prop_test::gen::strings::eascii::impl::Generator({.options= {.minLen = 0, .maxLen = 0}, .allowedChars = {}});
}
