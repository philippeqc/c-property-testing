#include <prop_test/gen/strings/unicode.h>
#include "ranges.h"
#include <random>

namespace prop_test::gen::strings::unicode::impl {
  static auto char_from_ranges(const std::vector<prop_test::gen::strings::ranges::Range<uint32_t>> &chars, size_t index) -> uint32_t {
    if (chars.empty()) {
      return 0;
    }

    size_t rangeIndex = 0;
    auto curRange = chars[rangeIndex];
    size_t totalSize = 0;
    while (index >= curRange.size()) {
      index -= curRange.size();
      totalSize += curRange.size();

      if (rangeIndex + 1 < chars.size()) {
        curRange = chars[++rangeIndex];
      } else {
        return char_from_ranges(chars, index % totalSize);
      }
    }

    return curRange.begin + index;
  }

  struct MultiByteChar {
    size_t numBytesUsed;
    std::array<char8_t, 4> bytes;

    constexpr auto emplace_back(char8_t elem) -> void {
      if (numBytesUsed + 1 > bytes.size()) {
        throw std::out_of_range{"MultiByteChar emplace_back out of bounds"};
      }
      bytes[numBytesUsed++] = std::move(elem);
    }

    auto begin() const noexcept { return bytes.begin(); }
    auto end() const noexcept { return bytes.end(); }
    auto begin() noexcept { return bytes.begin(); }
    auto end() noexcept { return bytes.end(); }
  };

  static constexpr auto runeMasks = std::array{
    0b111110000000000000000,
    0b000001111100000000000,
    0b000000000011110000000,
  };

  static auto rune_to_utf8(char32_t rune) -> MultiByteChar {
    auto bytes = MultiByteChar{};

    auto fourBytes = rune & runeMasks[0];
    auto threeBytes = rune & runeMasks[1];
    auto twoBytes = rune & runeMasks[2];

    if (fourBytes != 0U) {
      auto byte1 = static_cast<char8_t>((rune & 0b111000000000000000000) >> 18);
      auto byte2 = static_cast<char8_t>((rune & 0b000111111000000000000) >> 12);
      auto byte3 = static_cast<char8_t>((rune & 0b000000000111111000000) >> 6);
      auto byte4 = static_cast<char8_t>(rune & 0b000000000000000111111);
      bytes.emplace_back(0b11110000 | byte1);
      bytes.emplace_back(0b10000000 | byte2);
      bytes.emplace_back(0b10000000 | byte3);
      bytes.emplace_back(0b10000000 | byte4);
    } else if (threeBytes != 0U) {
      auto byte1 = static_cast<char8_t>((rune & 0b1111000000000000) >> 12);
      auto byte2 = static_cast<char8_t>((rune & 0b0000111111000000) >> 6);
      auto byte3 = static_cast<char8_t>((rune & 0b0000000000111111));
      bytes.emplace_back(0b11100000 | byte1);
      bytes.emplace_back(0b10000000 | byte2);
      bytes.emplace_back(0b10000000 | byte3);
    } else if (twoBytes != 0U) {
      auto byte1 = static_cast<char8_t>((rune & 0b11111000000) >> 6);
      auto byte2 = static_cast<char8_t>(rune & 0b00000111111);
      bytes.emplace_back(0b11000000 | byte1);
      bytes.emplace_back(0b10000000 | byte2);
    } else {
      bytes.emplace_back(static_cast<char8_t>(rune));
    }
    return bytes;
  }

  // The utf8 encoding algorithm is from my work-in-progress unicode library at https://gitlab.com/mtolman1/unicode/-/tree/remove-unicode-download?ref_type=heads
  static auto utf8_encode(const std::vector<uint32_t> &runes) -> std::string {
    std::string s = {};
    // Initial guess
    s.reserve(runes.size());

    bool first = true;
    for (auto rune : runes) {
      first = false;
      auto bytes = rune_to_utf8(rune);
      std::copy(bytes.begin(), bytes.end(), std::back_inserter(s));
    }

    s.shrink_to_fit();
    return s;
  }


  constexpr uint8_t continueFlag = 0b10000000;
  constexpr auto utf8StartFlags = std::array<std::tuple<uint8_t, int, uint8_t>, 5>{
    std::make_tuple(0b11110000, 4, 0b00000111),
    std::make_tuple(0b11100000, 3, 0b00001111),
    std::make_tuple(0b11000000, 2, 0b00011111),
    std::make_tuple(continueFlag, -1, 0b00111111),
    std::make_tuple(0b00000000, 1, 0b01111111),
  };

  auto num_bytes(char8_t byte) -> int {
    for (const auto [ flagSet, bytes, _flag_removal ] : utf8StartFlags) {
      if (byte >= flagSet) {
        return bytes;
      }
    }
    return -1;
  }

  auto without_flag(char8_t byte) -> uint8_t {
    for (const auto [ flagSet, _bytes, removeFlag ] : utf8StartFlags) {
      if (byte >= flagSet) {
        return byte & removeFlag;
      }
    }
    return byte;
  }

  auto get_flag(char8_t byte) -> uint8_t {
    for (const auto [ flagSet, _bytes, _removeFlag ] : utf8StartFlags) {
      if (byte >= flagSet) {
        return flagSet;
      }
    }
    return 0;
  }


  // The utf8 iterator is from my work-in-progress unicode library at https://gitlab.com/mtolman1/unicode/-/tree/remove-unicode-download?ref_type=heads
  /**
   * Read iterator for strings which will iterate over a string getting all of the runes
   * Assumes UTF-8 encoding for strings
   */
  class StrToUtf32Iterator {
  private:
    const std::string_view &strRef;
    size_t strIndex = 0;
    std::tuple<char32_t, size_t> currentRunePos;

    auto read_rune() -> void {
      size_t   curIndex    = strIndex;
      char32_t currentRune = 0;
      if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
      }
      else if (strIndex < strRef.size()) {
        auto numBytes = num_bytes(strRef[ curIndex ]);
        if (numBytes < 0) {
          // If the character is corrupted, return a null character
          strIndex++;
        }
        else if (curIndex + numBytes - 1 >= strRef.size()) {
          // If the character goes past the end of the string, return a null character and jump to the end
          strIndex = strRef.size();
        }
        else {
          strIndex = curIndex + numBytes;
          for (size_t index = curIndex; index < strIndex; ++index) {
            auto flag = get_flag(strRef[ index ]);
            if (index > curIndex && flag != continueFlag) {
              // Premature unicode character ending
              strIndex    = curIndex + index - 1;
              currentRune = 0;
              break;
            }
            auto noFlag = without_flag(strRef[ index ]);
            currentRune <<= 6;
            currentRune |= noFlag;
          }
        }
      }
      currentRunePos = std::make_tuple(currentRune, curIndex);
    }

  public:
    using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
    using difference_type [[maybe_unused]] = std::ptrdiff_t;
    using value_type = char32_t;
    using pointer = const std::tuple<char32_t, size_t> *;  // or also value_type*
    using reference = const std::tuple<char32_t, size_t> &;// or also value_type&

    StrToUtf32Iterator(const std::string_view &str, bool end)  : strRef(str) {
      if (end) {
        strIndex = strRef.size() + 1;
      }
      else {
        read_rune();
      }
    }

    auto operator<=>(const StrToUtf32Iterator &o) const -> std::partial_ordering {
      if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
      }
      return strIndex <=> o.strIndex;
    }

    auto operator==(const StrToUtf32Iterator &o) const -> bool { return (*this <=> o) == std::partial_ordering::equivalent; }

    auto operator!=(const StrToUtf32Iterator &o) const -> bool { return (*this <=> o) != std::partial_ordering::equivalent; }

    auto operator<(const StrToUtf32Iterator &o) const -> bool { return (*this <=> o) == std::partial_ordering::less; }

    auto operator>(const StrToUtf32Iterator &o) const -> bool { return (*this <=> o) == std::partial_ordering::greater; }

    auto operator<=(const StrToUtf32Iterator &o) const -> bool {
      auto cmp = (*this <=> o);
      return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
    }

    auto operator>=(const StrToUtf32Iterator &o) const -> bool {
      auto cmp = (*this <=> o);
      return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
    }

    auto operator*() const -> reference { return currentRunePos; }

    auto operator->() const -> pointer { return &currentRunePos; }

    auto operator++() -> StrToUtf32Iterator & {
      read_rune();
      return *this;
    }

    auto operator++(int) -> StrToUtf32Iterator {
      auto cpy = *this;
      ++(*this);
      return cpy;
    }
  };
}
auto prop_test::gen::strings::unicode::from_categories(const std::vector<Category> &categories, Options options) -> prop_test::gen::strings::unicode::impl::Generator {
  auto availRanges = std::vector<ranges::Range<uint32_t>>{};
  for (const auto& category : categories) {
    auto rngs = ranges::impl::unicode::category_ranges(category);
    std::copy(rngs.begin(), rngs.end(), std::back_inserter(availRanges));
  }
  return prop_test::gen::strings::unicode::impl::Generator{
    .availableRanges = availRanges,
    .options = options,
  };
}
auto prop_test::gen::strings::unicode::from_category(prop_test::gen::strings::unicode::Category category, prop_test::gen::strings::Options options) -> prop_test::gen::strings::unicode::impl::Generator {
  return from_categories({category}, options);
}
auto prop_test::gen::strings::unicode::impl::Generator::create(size_t index) const -> Generator::type {
  auto rnd = std::mt19937_64(index);
  size_t len;
  if (options.maxLen <= options.minLen) {
    const auto _ = rnd();
    len = options.minLen;
  }
  else {
    len = (rnd() % (options.maxLen - options.minLen + 1)) + options.minLen;
  }
  auto res = std::vector<uint32_t>{};
  res.resize(len);

  for (auto& ch : res) {
    ch = char_from_ranges(availableRanges, rnd());
  }

  return utf8_encode(res);
}

auto prop_test::gen::strings::unicode::impl::Generator::simplify_index(const size_t &failedIndex) const -> Simplification<Generator::type, impl::Simplifier> {
  return Simplification<Generator::type, impl::Simplifier>{
    .value = create(failedIndex),
    .next = {
      .availableRanges=availableRanges,
      .options=options,
      .index=failedIndex
    }
  };
}

auto prop_test::gen::strings::unicode::impl::Simplifier::branches(const type &val) const -> std::vector<Simplification<type, Simplifier>> {
  auto res = std::vector<Simplification<type, Simplifier>>{};

  if (options.maxLen > options.minLen) {
    res.emplace_back(Generator{.availableRanges=availableRanges,.options={.minLen=options.minLen, .maxLen=options.maxLen-1},}.simplify_index(index));
  }
  if (availableRanges.size() > 1) {
    for (size_t dropIndex = 0; dropIndex < availableRanges.size(); ++dropIndex) {
      decltype(availableRanges) newRanges = {};
      newRanges.resize(availableRanges.size() - 1);
      for (size_t copyIndex = 0, destIndex = 0; copyIndex < availableRanges.size(); ++copyIndex) {
        if (copyIndex == dropIndex) {
          continue;
        }
        newRanges[destIndex++] = availableRanges[copyIndex];
      }
      res.emplace_back(Generator{.availableRanges=newRanges,.options=options}.simplify_index(index));
    }
  }

  return res;
}
