#include "common.h"

auto prop_test::gen::faker::common::replace(std::string &str, const std::string &from, const std::string &to) -> void {
  size_t start_pos = str.find(from);
  if(start_pos == std::string::npos) {
    return;
  }
  str.replace(start_pos, from.length(), to);
}
