#include "common.h"
#include <prop_test/gen/faker/computers.h>

#define PROP_TEST_COMPUTER_DEF(NAME)                                                                                                                        \
  static const auto NAME##_data = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_##NAME);                                        \
  auto prop_test::gen::faker::computers::NAME(prop_test::DataVersion dataVersion)->prop_test::gen::faker::impl::IndexGenerator<std::string> { \
    return common::index_generator(                                                                                                                         \
      +[](size_t index, DataVersion dataVersion) -> std::string {                                                                                           \
        return common::at_version_index(index, dataVersion, NAME##_data)->string;                                                                           \
      },                                                                                                                                                    \
      dataVersion);                                                                                                                                         \
  }

PROP_TEST_COMPUTER_DEF(cmake_system_names)
PROP_TEST_COMPUTER_DEF(cpu_architectures)
PROP_TEST_COMPUTER_DEF(file_extensions)

static const auto file_mime_mapping_data = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_file_mime_mapping);
auto prop_test::gen::faker::computers::file_mime_mapping(prop_test::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<MimeMapping> {
  return common::index_generator(
    +[](size_t index, DataVersion dataVersion) -> MimeMapping {
      const auto* val = common::at_version_index(index, dataVersion, file_mime_mapping_data);
      return {
        .mimeType = val->mimeType,
        .extension = val->extension
      };
    }, dataVersion);
}

PROP_TEST_COMPUTER_DEF(file_types)
PROP_TEST_COMPUTER_DEF(mime_types)

static const auto operating_systems_data = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_os);
auto prop_test::gen::faker::computers::operating_systems(prop_test::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<std::string> {
  return common::index_generator(
    +[](size_t index, DataVersion dataVersion) -> std::string { return common::at_version_index(index, dataVersion, operating_systems_data)->string; }, dataVersion);
}

#undef PROP_TEST_COMPUTER_DEF
