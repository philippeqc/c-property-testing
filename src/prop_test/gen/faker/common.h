#pragma once

#include <prop_test/gen/faker/common.h>
#include <faker-data/faker-data.h>
#include <vector>
#include <array>
#include <sstream>

namespace prop_test::gen::faker::common {
  template<typename T>
  auto make_version_indices_for_method(T*(*method)(size_t *outNumEntries)) {
    auto versions = std::array<std::vector<T*>, static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST) + 1>{};

    size_t numElems;
    const auto* values = method(&numElems);

    for (size_t i = 0; i < numElems; ++i) {
      for (size_t v = 0; v <= static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST); ++v) {
        if (faker_data_is_active(static_cast<FakerDataVersionNumber>(v), &values[i].versionInfo)) {
          versions[v].emplace_back(&values[i]);
        }
      }
    }
    return versions;
  }

  template<typename T, size_t N>
  auto at_version_index(size_t index, DataVersion version, const std::array<std::vector<T>,N>& array) -> const T& {
    const auto& vec = array[static_cast<size_t>(version)];
    return vec[index % vec.size()];
  }

  template<typename T>
  auto make_complexity_indices_for_method(T*(*method)(size_t *outNumEntries)) {
    auto versions = std::array<
      std::array<
        std::vector<T*>,
        (static_cast<size_t>(prop_test::gen::faker::DataComplexity::COMPLEX) + 1)
      >,
      (static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST) + 1)
    >{};

    size_t numElems;
    const auto* values = method(&numElems);

    for (size_t i = 0; i < numElems; ++i) {
      for (size_t v = 0; v <= static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST); ++v) {
        if (faker_data_is_active(static_cast<FakerDataVersionNumber>(v), &values[i].versionInfo)) {
          versions[v][values[i].complexity].emplace_back(&values[i]);
        }
      }
    }
    return versions;
  }

  template<typename T, size_t N, size_t N2>
  auto at_complexity_index(size_t index, DataVersion version, DataComplexity complexity, const std::array<std::array<std::vector<T>, N>,N2>& array) {
    const auto& vec = array[static_cast<size_t>(version)][static_cast<size_t>(complexity)];
    return vec[index % vec.size()];
  }

  auto replace(std::string& str, const std::string& from, const std::string& to) -> void;

  inline auto hex_stream(int v, std::ostream& ss) {
    static constexpr auto strs = "0123456789ABCDEF";
    auto lower = v & 0b1111;
    auto upper = (v >> 4) & 0b1111;
    ss << strs[upper] << strs[lower];
  }

  inline auto hex_str(int v) {
    std::stringstream ss{};
    hex_stream(v, ss);
    return ss.str();
  }
}

