#include <prop_test/gen/faker/colors.h>
#include "common.h"
#include <sstream>
#include<iomanip>

static const auto colors_by_version = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_colors);
auto prop_test::gen::faker::colors::impl::named_color(size_t index, prop_test::gen::faker::DataVersion dataVersion) -> std::string {
  const auto& vec = colors_by_version[static_cast<size_t>(dataVersion)];
  return vec[index % vec.size()]->name;
}

auto prop_test::gen::faker::colors::RGB::css() const -> std::string {
  std::stringstream ss;
  ss << "rgb(" << static_cast<int>(red)
     << ", " << static_cast<int>(green)
     << ", " << static_cast<int>(blue)
     << ")";
  return ss.str();
}

auto prop_test::gen::faker::colors::RGB::hex(std::optional<char> prefix) const -> std::string {
  std::stringstream ss;
  if (prefix) {
    ss << *prefix;
  }
  common::hex_stream(red, ss);
  common::hex_stream(green, ss);
  common::hex_stream(blue, ss);
  return ss.str();
}

auto prop_test::gen::faker::colors::RGBA::css() const -> std::string {
  std::stringstream ss;
  ss << "rgba(" << static_cast<int>(red)
     << ", " << static_cast<int>(green)
     << ", " << static_cast<int>(blue)
     << ", " << static_cast<int>(alpha)
     << ")";
  return ss.str();
}

auto prop_test::gen::faker::colors::RGBA::hex(std::optional<char> prefix) const -> std::string {
  std::stringstream ss;
  if (prefix) {
    ss << *prefix;
  }
  common::hex_stream(red, ss);
  common::hex_stream(green, ss);
  common::hex_stream(blue, ss);
  common::hex_stream(alpha, ss);
  return ss.str();
}

auto prop_test::gen::faker::colors::HSL::css() const -> std::string {
  std::stringstream ss;
  ss << "hsl(" << std::to_string(hue) << "deg,"
     << " " << std::to_string(saturation * 100) << "%,"
     << " " << std::to_string(luminosity * 100) << "%"
     << ")";
  return ss.str();
}

auto prop_test::gen::faker::colors::HSLA::css() const -> std::string {
  std::stringstream ss;
  ss << "hsla(" << std::to_string(hue) << "deg,"
     << " " << std::to_string(saturation * 100) << "%,"
     << " " << std::to_string(luminosity * 100) << "%,"
     << " " << std::to_string(alpha)
     << ")";
  return ss.str();
}
