#pragma once

#include "common.h"
#include <array>
#include <bitset>
#include <prop_test/core.h>
#include <string>
#include <tuple>
#include <vector>

namespace prop_test::gen::strings::unicode {
  namespace impl {
    struct Generator;
  }

  enum class Category {
    Cc,
    Cf,
    Co,
    Cs,
    Ll,
    Lm,
    Lo,
    Lt,
    Lu,
    Mc,
    Me,
    Mn,
    Nd,
    Nl,
    No,
    Pc,
    Pd,
    Pe,
    Pf,
    Pi,
    Po,
    Ps,
    Sc,
    Sk,
    Sm,
    So,
    Zl,
    Zp,
    Zs,
  };

  auto from_categories(const std::vector<Category> &categories, Options options = {}) -> impl::Generator;

  auto from_category(Category category, Options options = {}) -> impl::Generator;

  namespace impl {
    struct Simplifier {
      using type = std::string;
      std::vector<ranges::Range<uint32_t>> availableRanges;
      Options options;
      size_t index;

      [[nodiscard]] auto prefer(const type &l, const type &r) const -> type { return l.size() < r.size() ? l : r; }
      [[nodiscard]] auto branches(const type &val) const -> std::vector<Simplification<type, Simplifier>>;
    };

    struct Generator {
      using type = std::string;
      std::vector<ranges::Range<uint32_t>> availableRanges;
      Options options;
      [[nodiscard]] auto create(size_t index) const -> type;
      [[nodiscard]] auto simplify_index(const size_t &index) const -> Simplification<type, Simplifier>;
    };
  };
}