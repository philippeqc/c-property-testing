#pragma once

#include <set>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a set of random elements
   *
   * **Warning!** Does not shrink!
   * @tparam Gen Type of ints
   * @param generator Generator for elements
   * @param maxLen Maximum length of the set *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename Gen>
  auto set(const Gen& generator, size_t maxSize = std::numeric_limits<unsigned char>::max());

  namespace impl::set {
    template<typename T>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::set<T> &l, const std::set<T> &r) const -> const std::set<T>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::set<T> &val) const -> std::vector<Simplification<std::set<T>, Simplifier>> { return {}; }
    };

    template<typename BaseGenerator>
    struct Generator {
      using T = typename prop_test::impl::GenCreationType<BaseGenerator>::type ;
      BaseGenerator generator;

      size_t maxSize;
      [[nodiscard]] auto create(size_t index) const -> std::set<T> {
        if (maxSize == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::set<T>{};
        auto size = rnd() % maxSize;
        for (size_t i = 0; i < size; ++i) {
          res.insert(generator.create(rnd()));
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::set<T> &failed) const -> Simplification<std::set<T>, Simplifier<T>> {
        return Simplification<std::set<T>, Simplifier<T>>{
          .value = failed,
          .next = Simplifier<T>{}};
      }
    };
  }

  template<typename Gen>
  auto set(const Gen& generator, size_t maxSize) {
    return impl::set::Generator<Gen>{.generator=generator, .maxSize = maxSize};
  }
}
