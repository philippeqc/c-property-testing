#pragma once

#include <unordered_map>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a unordered_map of random elements
   *
   * **Warning!** Does not shrink!
   * @tparam KGen Type of key ints
   * @tparam VGen Type of value ints
   * @param keyGenerator Generator for element keys
   * @param valueGenerator Generator for element values
   * @param maxLen Maximum length of the unordered_map *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename KGen, typename VGen>
  auto unordered_map(const KGen& keyGenerator, const VGen& valueGenerator, size_t maxSize = std::numeric_limits<unsigned char>::max());

  namespace impl::unordered_map {
    template<typename K, typename V>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::unordered_map<K, V> &l, const std::unordered_map<K, V> &r) const -> const std::unordered_map<K, V>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::unordered_map<K, V> &val) const -> std::vector<Simplification<std::unordered_map<K, V>, Simplifier>> { return {}; }
    };

    template<typename KeyGenerator, typename ValueGenerator>
    struct Generator {
      using V = typename prop_test::impl::GenCreationType<ValueGenerator>::type;
      using K = typename prop_test::impl::GenCreationType<KeyGenerator>::type;
      ValueGenerator valueGenerator;
      KeyGenerator keyGenerator;

      size_t maxSize;
      [[nodiscard]] auto create(size_t index) const -> std::unordered_map<K, V> {
        if (maxSize == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::unordered_map<K, V>{};
        auto size = rnd() % maxSize;
        for (size_t i = 0; i < size; ++i) {
          res[keyGenerator.create(rnd())] = valueGenerator.create(rnd());
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::unordered_map<K, V> &failed) const -> Simplification<std::unordered_map<K, V>, Simplifier<K, V>> {
        return Simplification<std::unordered_map<K, V>, Simplifier<K, V>>{
          .value = failed,
          .next = Simplifier<K, V>{}};
      }
    };
  }

  template<typename KGen, typename VGen>
  auto unordered_map(const KGen& keyGenerator, const VGen& valueGenerator, size_t maxSize) {
    return impl::unordered_map::Generator<KGen, VGen>{.valueGenerator=valueGenerator, .keyGenerator=keyGenerator, .maxSize = maxSize};
  }
}
