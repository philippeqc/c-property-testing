#pragma once

#include <prop_test/gen/utils/combine.h>
#include <prop_test/common.h>
#include <tuple>

namespace prop_test::gen::collections {
  /**
   * Creates a tuple with random elements
   * @tparam Generators Types of generators to use
   * @param generators Generators to use
   * @return
   */
  template<typename... Generators>
  auto tuple(const Generators &...generators) {
    return utils::combine<std::tuple<typename prop_test::impl::GenCreationType<Generators>::type...>>(generators...);
  }
}// namespace prop_test::gen::utils
