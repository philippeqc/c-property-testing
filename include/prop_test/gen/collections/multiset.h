#pragma once

#include <set>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a multiset of random elements
   *
   * **Warning!** Does not shrink!
   * @tparam Gen Type of ints
   * @param generator Generator for elements
   * @param maxLen Maximum length of the multiset *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename Gen>
  auto multiset(const Gen& generator, size_t maxSize = std::numeric_limits<unsigned char>::max());

  namespace impl::multiset {
    template<typename T>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::multiset<T> &l, const std::multiset<T> &r) const -> const std::multiset<T>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::multiset<T> &val) const -> std::vector<Simplification<std::multiset<T>, Simplifier>> { return {}; }
    };

    template<typename BaseGenerator>
    struct Generator {
      using T = typename prop_test::impl::GenCreationType<BaseGenerator>::type ;
      BaseGenerator generator;

      size_t maxSize;
      [[nodiscard]] auto create(size_t index) const -> std::multiset<T> {
        if (maxSize == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::multiset<T>{};
        auto size = rnd() % maxSize;
        for (size_t i = 0; i < size; ++i) {
          res.insert(generator.create(rnd()));
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::multiset<T> &failed) const -> Simplification<std::multiset<T>, Simplifier<T>> {
        return Simplification<std::multiset<T>, Simplifier<T>>{
          .value = failed,
          .next = Simplifier<T>{}};
      }
    };
  }

  template<typename Gen>
  auto multiset(const Gen& generator, size_t maxSize) {
    return impl::multiset::Generator<Gen>{.generator=generator, .maxSize = maxSize};
  }
}
