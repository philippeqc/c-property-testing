#pragma once

#include <bitset>
#include <limits>
#include "prop_test/common.h"
#include <prop_test/gen/strings/ascii.h>

namespace prop_test::gen::collections {
  /**
   * Generates an bitset of random elements
   *
   * **Does not shrink output!***
   *
   * @tparam N Size of the bitset
   * @return
   */
  template<size_t N>
  auto bitset();

  namespace impl::bitset {
    template<size_t N>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::bitset<N> &l, const std::bitset<N> &r) const -> const std::bitset<N>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::bitset<N> &val) const -> std::vector<Simplification<std::bitset<N>, Simplifier>> { return {}; }
    };

    template<size_t N>
    struct Generator {
      [[nodiscard]] auto create(size_t index) const -> std::bitset<N> {
        if (N == 0) {
          return {};
        }
        return std::bitset<N>{gen::strings::ascii::binary({.minLen=N, .maxLen=N}).create(index)};
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::bitset< N> &failed) const -> Simplification<std::bitset<N>, Simplifier<N>> {
        return Simplification<std::bitset<N>, Simplifier<N>>{
          .value = failed,
          .next = Simplifier<N>{}};
      }
    };
  }

  template<size_t N>
  auto bitset() {
    return impl::bitset::Generator<N>{};
  }
}
