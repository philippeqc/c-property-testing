#pragma once

#include <map>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a multimap of random elements
   *
   * **Warning!** Does not shrink!
   * @tparam KGen Type of key ints
   * @tparam VGen Type of value ints
   * @param keyGenerator Generator for element keys
   * @param valueGenerator Generator for element values
   * @param maxLen Maximum length of the multimap *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename KGen, typename VGen>
  auto multimap(const KGen& keyGenerator, const VGen& valueGenerator, size_t maxSize = std::numeric_limits<unsigned char>::max());

  namespace impl::multimap {
    template<typename K, typename V>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::multimap<K, V> &l, const std::multimap<K, V> &r) const -> const std::multimap<K, V>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::multimap<K, V> &val) const -> std::vector<Simplification<std::multimap<K, V>, Simplifier>> { return {}; }
    };

    template<typename KeyGenerator, typename ValueGenerator>
    struct Generator {
      using V = typename prop_test::impl::GenCreationType<ValueGenerator>::type;
      using K = typename prop_test::impl::GenCreationType<KeyGenerator>::type;
      ValueGenerator valueGenerator;
      KeyGenerator keyGenerator;

      size_t maxSize;
      [[nodiscard]] auto create(size_t index) const -> std::multimap<K, V> {
        if (maxSize == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::multimap<K, V>{};
        auto size = rnd() % maxSize;
        for (size_t i = 0; i < size; ++i) {
          res.emplace(keyGenerator.create(rnd()), valueGenerator.create(rnd()));
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::multimap<K, V> &failed) const -> Simplification<std::multimap<K, V>, Simplifier<K, V>> {
        return Simplification<std::multimap<K, V>, Simplifier<K, V>>{
          .value = failed,
          .next = Simplifier<K, V>{}};
      }
    };
  }

  template<typename KGen, typename VGen>
  auto multimap(const KGen& keyGenerator, const VGen& valueGenerator, size_t maxSize) {
    return impl::multimap::Generator<KGen, VGen>{.valueGenerator=valueGenerator, .keyGenerator=keyGenerator, .maxSize = maxSize};
  }
}
