#pragma once

#include <vector>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a vector of random elements
   * @tparam Gen Type of ints
   * @param generator Generator for elements
   * @param maxLen Maximum length of the vector *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename Gen>
  auto vector(const Gen& generator, size_t maxLen = std::numeric_limits<unsigned char>::max());

  namespace impl::vector {
    template<typename T>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::vector<T> &l, const std::vector<T> &r) const -> const std::vector<T>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::vector<T> &val) const -> std::vector<Simplification<std::vector<T>, Simplifier>> {
        if (val.empty()) {
          return {};
        }

        auto cpy = val;
        cpy.pop_back();
        return std::vector<Simplification<std::vector<T>, Simplifier>>{
          Simplification<std::vector<T>, Simplifier>{.next={}, .value=cpy}
        };
      }
    };

    template<typename BaseGenerator>
    struct Generator {
      using T = typename prop_test::impl::GenCreationType<BaseGenerator>::type ;
      BaseGenerator generator;

      size_t maxLen;
      [[nodiscard]] auto create(size_t index) const -> std::vector<T> {
        if (maxLen == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::vector<T>{};
        res.resize(rnd() % maxLen);
        for (auto& elem : res) {
          elem = generator.create(rnd());
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::vector<T> &failed) const -> Simplification<std::vector<T>, Simplifier<T>> {
        return Simplification<std::vector<T>, Simplifier<T>>{
          .value = failed,
          .next = Simplifier<T>{}};
      }
    };
  }

  template<typename Gen>
  auto vector(const Gen& generator, size_t maxLen) {
    return impl::vector::Generator<Gen>{.generator=generator, .maxLen = maxLen};
  }
}
