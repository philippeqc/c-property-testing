#pragma once

#include <array>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates an array of random elements
   *
   * **Does not shrink output!***
   *
   * @tparam N Size of the array
   * @tparam Gen Type of ints for elements
   * @param generator Generator for elements
   * @return
   */
  template<size_t N, typename Gen>
  auto array(const Gen& generator);

  namespace impl::array {
    template<size_t N, typename T>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::array<T, N> &l, const std::array<T, N> &r) const -> const std::array<T, N>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::array<T, N> &val) const -> std::vector<Simplification<std::array<T, N>, Simplifier>> { return {}; }
    };

    template<size_t N, typename BaseGenerator>
    struct Generator {
      using T = typename prop_test::impl::GenCreationType<BaseGenerator>::type ;
      BaseGenerator generator;

      [[nodiscard]] auto create(size_t index) const -> std::array<T, N> {
        if (N == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::array<T, N>{};
        for (auto& elem : res) {
          elem = generator.create(rnd());
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::array<T, N> &failed) const -> Simplification<std::array<T, N>, Simplifier<N, T>> {
        return Simplification<std::array<T, N>, Simplifier<N, T>>{
          .value = failed,
          .next = Simplifier<N, T>{}};
      }
    };
  }

  template<size_t N, typename Gen>
  auto array(const Gen& generator) {
    return impl::array::Generator<N, Gen>{.generator=generator};
  }
}
