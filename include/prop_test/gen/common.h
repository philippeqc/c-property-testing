#pragma once

#include <vector>
#include <prop_test/core.h>

namespace prop_test::gen::common {
  namespace impl {
    template<typename T>
    struct NoShrinkSimplifier {
      [[nodiscard]] auto prefer(const T &l, const T &r) const -> T { return r; }
      [[nodiscard]] auto branches(const T &val) const -> std::vector<Simplification<T, NoShrinkSimplifier>> { return {}; }
    };

    template<typename T>
    struct NoShrinkGenerator {
      using type = T;

      [[nodiscard]] auto create(size_t index) const -> type { return fp(index); }
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<type, NoShrinkSimplifier<type>> {
        return {
          .value = failed,
          .next = NoShrinkSimplifier<type>{}};
      }
      std::function<T (size_t)> fp;
    };
  }

  template<typename FP>
  auto no_shrink_generator(const FP& function) {
    return impl::NoShrinkGenerator<std::invoke_result_t<FP, size_t>>{function};
  }
}
