#pragma once

#include <string>
#include <optional>
#include <prop_test/gen/strings/ascii.h>
#include <prop_test/gen/integer.h>
#include <prop_test/gen/fp.h>
#include "common.h"
#include <prop_test/gen/utils/combine.h>

namespace prop_test::gen::faker::colors {
  /**
   * Represents Red, Green, Blue
   */
  struct RGB {
    uint8_t red;
    uint8_t green;
    uint8_t blue;

    /**
     * Returns the CSS representation of rgb
     * @return
     */
    [[nodiscard]] auto css() const -> std::string;

    /**
     * Returns the color as hex
     * @param prefix Prefix to use (defaults ot '#')
     * @return
     */
    [[nodiscard]] auto hex(std::optional<char> prefix = '#') const -> std::string;

    /**
     * Operator to check for equivalence
     * @param o
     * @return
     */
    [[nodiscard]] auto operator==(const RGB& o) const {
      return red == o.red && green == o.green && blue == o.blue;
    }

    /**
     * Operator to check for inequality
     * @param o
     * @return
     */
    [[nodiscard]] auto operator!=(const RGB& o) const { return !(*this == o); }
  };

  /**
   * Represents Red, Green, Blue, Alpha
   */
  struct RGBA {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t alpha;

    /**
     * Returns the CSS representation of rgb
     * @return
     */
    [[nodiscard]] auto css() const -> std::string;

    /**
     * Returns the color as hex
     * @param prefix Prefix to use (defaults ot '#')
     * @return
     */
    [[nodiscard]] auto hex(std::optional<char> prefix = '#') const -> std::string;

    /**
     * Operator to check for equivalence
     * @param o
     * @return
     */
    [[nodiscard]] auto operator==(const RGBA& o) const {
      return red == o.red && green == o.green && blue == o.blue && alpha == o.alpha;
    }

    /**
     * Operator to check for inequality
     * @param o
     * @return
     */
    [[nodiscard]] auto operator!=(const RGBA& o) const { return !(*this == o); }
  };

  /**
   * Represents Hue, Saturation, Luminosity
   */
  struct HSL {
    double hue;
    double saturation;
    double luminosity;

    /**
     * Css representation of HSL
     * @return
     */
    [[nodiscard]] auto css() const -> std::string;
  };

  /**
   * Represents Hue, Saturation, Luminosity, Alpha
   */
  struct HSLA {
    double hue;
    double saturation;
    double luminosity;
    double alpha;

    /**
     * Css representation of HSLA
     * @return
     */
    [[nodiscard]] auto css() const -> std::string;
  };

  namespace impl {
    template<typename T>
    using IndexGenerator = ::prop_test::gen::faker::impl::IndexGenerator<T>;
    auto named_color(size_t index, DataVersion dataVersion = default_version()) -> std::string;
  }

  /**
   * Creates a ints for color names
   * @param dataVersion Version of data faker data to use
   * @return
   */
  inline auto name_generator(DataVersion dataVersion = default_version()) -> impl::IndexGenerator<std::string> {
    return common::index_generator(impl::named_color, dataVersion);
  }

  /**
   * Generator for RGB hex color
   * @return
   */
  inline auto hex_generator() -> strings::ascii::impl::Generator { return gen::strings::ascii::hex({.minLen=6, .maxLen=6}); }

  /**
   * Generator for RGBA hex color
   * @return
   */
  inline auto hex_alpha_generator() -> strings::ascii::impl::Generator { return gen::strings::ascii::hex({.minLen=8, .maxLen=8}); }

  /**
   * Generator for RGB colors
   * @return
   */
  inline auto rgb_generator() {
    return gen::utils::combine<RGB>(
      gen::integer::ints<uint8_t>(),
      gen::integer::ints<uint8_t>(),
      gen::integer::ints<uint8_t>()
    );
  }

  /**
   * Generator for RGBA colors
   * @return
   */
  inline auto rgba_generator() {
    return gen::utils::combine<RGBA>(
      gen::integer::ints<uint8_t>(),
      gen::integer::ints<uint8_t>(),
      gen::integer::ints<uint8_t>(),
      gen::integer::ints<uint8_t>()
    );
  }

  /**
   * Generator for HSL colors
   * @return
   */
  inline auto hsl_generator() {
    return gen::utils::combine<HSL>(
      fp::floats<double>({.min = 0, .max = 360}),
      fp::floats<double>({.min = 0, .max = 1}),
      fp::floats<double>({.min = 0, .max = 1})
    );
  }

  /**
   * Generator for HSLA colors
   * @return
   */
  inline auto hsla_generator() {
    return gen::utils::combine<HSLA>(
      fp::floats<double>({.min = 0, .max = 360}),
      fp::floats<double>({.min = 0, .max = 1}),
      fp::floats<double>({.min = 0, .max = 1}),
      fp::floats<double>({.min = 0, .max = 1})
    );
  }
}

template<>
struct std::hash<prop_test::gen::faker::colors::RGB> {
  auto operator()(const prop_test::gen::faker::colors::RGB& c) const noexcept {
    return static_cast<size_t>(c.red) << 16 | static_cast<size_t>(c.green) << 8 | static_cast<size_t>(c.blue);
  }
};

template<>
struct std::hash<prop_test::gen::faker::colors::RGBA> {
  auto operator()(const prop_test::gen::faker::colors::RGBA& c) const noexcept {
    return static_cast<size_t>(c.red) << 24 | static_cast<size_t>(c.green) << 16 | static_cast<size_t>(c.blue) << 8 | static_cast<size_t>(c.alpha);
  }
};

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::colors::RGB& d) -> std::ostream& {
  os << "RGB{.red=\"" << static_cast<int>(d.red) << "\",.green=\"" << static_cast<int>(d.green) << "\",.blue=\"" << static_cast<int>(d.blue) << "\"}";
  return os;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::colors::RGBA& d) -> std::ostream& {
  os << "RGBA{.red=\"" << static_cast<int>(d.red) << "\",.green=\"" << static_cast<int>(d.green) << "\",.blue=\"" << static_cast<int>(d.blue) << "\",.alpha=\"" << static_cast<int>(d.alpha) << "\"}";
  return os;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::colors::HSL& d) -> std::ostream& {
  os << "HSL{.hue=\"" << d.hue << "\",.saturation=\"" << d.saturation << "\",.luminosity=\"" << d.luminosity << "\"}";
  return os;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::colors::HSLA& d) -> std::ostream& {
  os << "HSLA{.hue=\"" << d.hue << "\",.saturation=\"" << d.saturation << "\",.luminosity=\"" << d.luminosity << "\",.alpha=\"" << d.alpha << "\"}";
  return os;
}
