#pragma once

#include <prop_test/core.h>
#include <vector>
#include <functional>
#include <ostream>
#include <prop_test/gen/common.h>

namespace prop_test::gen::faker {
  using DataVersion = prop_test::DataVersion;

  enum class DataComplexity {
    RUDIMENTARY,
    BASIC,
    INTERMEDIATE,
    ADVANCED,
    COMPLEX
  };

  struct ComplexityOptions {
    DataComplexity minComplexity = DataComplexity::RUDIMENTARY;
    DataComplexity maxComplexity = DataComplexity::COMPLEX;
    DataVersion version = default_version();
  };

  namespace common {
    /**
     * Creates a ints that retrieves items from a function by index
     * Note: This does NOT do simplification
     * @tparam FP Function type; return type of function will be the output of the ints
     * @param function Function to retrieve data by version
     * @param dataVersion Version of faker data
     * @return
     */
    template<typename FP>
    auto index_generator(const FP& function, DataVersion dataVersion = default_version());

    /**
     * Creates a ints that retrieves items from a function by index
     * Note: This does NOT do simplification
     * @tparam FP Function type; function result type will be the ints output type
     * @param function Function to retrieve data by version
     * @param minComplexity Minimum data complexity for the output
     * @param maxComplexity Maximum data complexity for the output
     * @param dataVersion Version of faker data
     * @return
     */
    template<typename FP>
    auto complexity_generator(const FP& function, ComplexityOptions options = {});

  }

  namespace impl {

    template<typename T>
    struct IndexSimplifier {
      [[nodiscard]] auto prefer(const T &l, const T &r) const -> T { return r; }
      [[nodiscard]] auto branches(const T &val) const -> std::vector<Simplification<T, IndexSimplifier>> { return {}; }
    };

    template<typename T>
    struct IndexGenerator {
      using type = T;

      [[nodiscard]] auto create(size_t index) const -> type { return fp(index, version); }
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<type, IndexSimplifier<type>> {
        return {
          .value = failed,
          .next = IndexSimplifier<type>{}};
      }
      std::function<T (size_t, DataVersion)> fp;
      DataVersion version;
    };

    static_assert(concepts::is_a_generator<IndexGenerator<int (*)(size_t, DataVersion)>>);

    template<typename T>
    struct ComplexitySimplifier {
      [[nodiscard]] auto prefer(const T &l, const T &r) const -> T { return r; }
      [[nodiscard]] auto branches(const T &val) const -> std::vector<Simplification<T, ComplexitySimplifier>> {
        return {};
      }
    };

    template<typename T>
    struct ComplexityGenerator {
      using type = T;

      [[nodiscard]] auto create(size_t index) const -> type {
        const auto numComplex = (static_cast<size_t>(options.maxComplexity) - static_cast<size_t>(options.minComplexity) + 1);
        auto complexity = static_cast<DataComplexity>(index % numComplex + static_cast<size_t>(options.minComplexity));
        auto newIndex = index / numComplex;
        return fp(newIndex, complexity, options.version);
      }
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<type, ComplexitySimplifier<type>> {
        return {
          .value = failed,
          .next = ComplexitySimplifier<type>{}};
      }
      std::function<type (size_t, DataComplexity, DataVersion)> fp;
      ComplexityOptions options = {};
    };

    static_assert(concepts::is_a_generator<ComplexityGenerator<int (*)(size_t, DataComplexity, DataVersion)>>);
  }

  namespace common {
    template<typename FP>
    auto index_generator(const FP& function, DataVersion dataVersion) {
      return impl::IndexGenerator<std::invoke_result_t<FP, size_t, DataVersion>>{function, dataVersion};
    }

    template<typename FP>
    auto complexity_generator(const FP& function, ComplexityOptions options) {
      return impl::ComplexityGenerator<std::invoke_result_t<FP, size_t, DataComplexity, DataVersion>>{
        .fp=function,
        .options = options,
      };
    }
  }
}
