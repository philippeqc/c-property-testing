#pragma once

#include "common.h"
#include <string>

namespace prop_test::gen::faker::computers {
  /**
   * Generator for CMake system names (e.g. Darwin, Windows, Linux)
   * @param dataVersion data version to use
   * @return
   */
  auto cmake_system_names(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for CPU architectures names (e.g. arm, x86_64, aarch64)
   * @param dataVersion data version to use
   * @return
   */
  auto cpu_architectures(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for file extensions, no period is part of he result (e.g. xls, doc)
   * @param dataVersion data version to use
   * @return
   */
  auto file_extensions(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Represents a common mime mapping between a mime type and a file extensions
   */
  struct MimeMapping {
    std::string mimeType;
    std::string extension;
  };

  /**
   * Generator for common mime mappings
   * @param dataVersion data version to use
   * @return
   */
  auto file_mime_mapping(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<MimeMapping>;

  /**
   * Generator for common file types
   * @param dataVersion data version to use
   * @return
   */
  auto file_types(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for common mime types
   * @param dataVersion data version to use
   * @return
   */
  auto mime_types(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for operating system names (e.g. windows, macos, linux, freebsd)
   * @param dataVersion data version to use
   * @return
   */
  auto operating_systems(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;
}
