#pragma once

#include <optional>
#include <string>
#include "prop_test/gen/faker/common.h"
#include <prop_test/gen/strings/ascii.h>

namespace prop_test::gen::faker::people {
  enum class Sex {
    MALE,
    FEMALE
  };

  /**
   * Represents a person
   */
  struct Person {
    /** Name prefix */
    std::optional<std::string> prefix;
    /** Name suffix */
    std::optional<std::string> suffix;
    /** Given name (first name in US) */
    std::string givenName;
    /** Family/surname (last name in US) */
    std::string surName;
    /** Full name; may be comprised of first/last or last/first */
    std::string fullName;
    /** Sex of person */
    Sex sex;
  };

  auto person(ComplexityOptions options = {}) -> impl::ComplexityGenerator<Person>;
  auto prefix(ComplexityOptions options = {}) -> impl::ComplexityGenerator<std::optional<std::string>>;
  auto suffix(ComplexityOptions options = {}) -> impl::ComplexityGenerator<std::optional<std::string>>;
  auto givenName(ComplexityOptions options = {}) -> impl::ComplexityGenerator<std::string>;
  auto surname(ComplexityOptions options = {}) -> impl::ComplexityGenerator<std::string>;
  auto fullName(ComplexityOptions options = {}) -> impl::ComplexityGenerator<std::string>;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::people::Person& d) -> std::ostream& {
  os << "Person{.prefix=\"" << d.prefix.value_or("<none>")
     << "\",.suffix=\"" << d.suffix.value_or("<none>")
     << "\",.givenName=\"" << d.givenName
     << "\",.surname=\"" << d.surName
     << "\",.fullName=\"" << d.fullName
     << "\",.sex=\"" << (d.sex == prop_test::gen::faker::people::Sex::FEMALE ? "FEMALE" : "MALE")
     << "\"}";
  return os;
}
