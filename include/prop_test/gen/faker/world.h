#pragma once

#include "common.h"

namespace prop_test::gen::faker::world {
  /**
   * Data for a country. Based off of ISO print from the CIA
   */
  struct Country {
    /* Name of the country */
    std::string name;
    /* ISO Alpha-3 code */
    std::string alpha3;
    /* ISO Alpha-2 code */
    std::string alpha2;
    /* ISO Numeric code */
    std::string numericCode;
    /* Top level domain */
    std::optional<std::string> domain;
  };

  auto country(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<Country>;

  auto timezone(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::world::Country& d) -> std::ostream& {
  os << "Country{"
     << ".name=\"" << d.name
     << "\",.alpha3=\"" << d.alpha3
     << "\",.alpha2=\"" << d.alpha2
     << "\",.numericCode=\"" << d.numericCode
     << "\",.domain=\"" << d.domain.value_or("<none>")
     << "\"}";
  return os;
}
