#pragma once

#include <cmath>
#include <concepts>
#include <prop_test/core.h>
#include <random>

namespace prop_test::gen::fp {
  /**
   * Floating point generation flags
   */
  enum FLAGS : unsigned {
    NONE = 0,
    ALLOW_NAN = 0x1 << 0,
    ALLOW_INFINITY = 0x1 << 1,
  };

  /**
   * Floating point generation settings
   * @tparam T The floating point type to simplify
   */
  template<std::floating_point T>
  struct Settings {
    T min = std::numeric_limits<T>::min();
    T max = std::numeric_limits<T>::max();
    FLAGS flags = FLAGS::NONE;
  };
  
  /**
   * Creates a floating point ints
   * @tparam T Type of floating point
   * @param options Options for generating floating point numbers
   * @return
   */
  template<std::floating_point T = double>
  auto floats(const Settings<T>& options = {});

  namespace impl::fp {
    /**
   * Floating point simplifier
   * @tparam T The floating point type to simplify
   */
    template<std::floating_point T>
    struct Simplifier {
      /** Float generation settings */
      Settings<T> settings;

      /**
     * Used to determine shrink preference
     * @param l Left operand
     * @param r Right operand
     * @return
     */
      [[nodiscard]] auto prefer(const T &l, const T &r) const -> T {
        return std::abs(l) < std::abs(r) ? l : r;
      }

      /**
     * Simplification branches
     * @param val Value to simplify
     * @return
     */
      [[nodiscard]] auto branches(const T &val) const -> std::vector<Simplification<T, Simplifier>> {
        if (std::isnan(val) || std::isinf(val) || val <= settings.min + std::numeric_limits<T>::epsilon() || val >= settings.max + std::numeric_limits<T>::epsilon()) {
          return {};
        }

        auto res = std::vector<Simplification<T, Simplifier>>{};
        auto down = (val + settings.min) / 2;
        auto up = (val + settings.max) / 2;
        if (down > settings.min + std::numeric_limits<T>::epsilon() && std::abs(down) <= std::abs(up)) {
          res.emplace_back(Simplification<T, Simplifier>{
            .value = down,
            .next = Simplifier{.settings = {.min = settings.min, .max = up}}});
        }

        if (up < settings.max - std::numeric_limits<T>::epsilon() && std::abs(down) >= std::abs(up)) {
          res.emplace_back(Simplification<T, Simplifier>{
            .value = up,
            .next = Simplifier{.settings = {.min = down, .max = settings.max}}});
        }

        res.shrink_to_fit();
        return res;
      }
    };

    /**
   * Floating point ints
   * @tparam T The floating point type to simplify
   */
    template<std::floating_point T>
    struct Generator {
      using type = T;

      /**
     * Creates a floating point number
     * @param index Seed for creation
     * @return
     */
      [[nodiscard]] auto create(size_t index) const -> T {
        if (settings.max <= settings.min) {
          return settings.min;
        }

        if constexpr (std::numeric_limits<T>::has_quiet_NaN) {
          if ((settings.flags & FLAGS::ALLOW_NAN) && (index % 100) == 0) {
            return std::numeric_limits<T>::quiet_NaN();
          }
        } else if constexpr (std::numeric_limits<T>::has_signaling_NaN) {
          if ((settings.flags & FLAGS::ALLOW_NAN) && (index % 100) == 0) {
            return std::numeric_limits<T>::signaling_NaN();
          }
        }

        if constexpr (std::numeric_limits<T>::has_infinity) {
          if (settings.flags & FLAGS::ALLOW_INFINITY) {
            if ((index % 100) == 1) {
              return std::numeric_limits<T>::infinity();
            } else if ((index % 100) == 2) {
              return -std::numeric_limits<T>::infinity();
            }
          }
        }
        auto rnd = std::mt19937_64(index);

        T val = rnd() / static_cast<T>(std::numeric_limits<decltype(rnd())>::max());
        const auto diff = (settings.max / 2 - settings.min / 2) * 2;
        return settings.min + diff * val;
      }

      /**
     * Gets simplifier for a value
     * @param failed Value to get simplifier for
     * @return
     */
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<T, Simplifier<T>> {
        return Simplification<T, Simplifier<T>>{
          .value = failed,
          .next = {
            .settings = settings}};
      }

      Settings<T> settings = {};
    };

    static_assert(prop_test::concepts::is_a_generator<Generator<double>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<float>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<long double>>);
  }

  template<std::floating_point T>
  auto floats(const Settings<T>& options) {
    return impl::fp::Generator<T>{.settings=options};
  }
}
