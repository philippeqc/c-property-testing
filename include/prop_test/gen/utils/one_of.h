#pragma once

#include <prop_test/core.h>
#include <functional>
#include <algorithm>
#include <variant>

namespace prop_test::gen::utils {
  /**
     * Generates values from any one of the provided generators
     * > **Note!** If the generators provide different types of values, then the output will be a variant of all possible value types!
     *
     * > **Warning!** Does not simplify!
     * @tparam Generator Base generator types
     * @param generator Base generators
     * @return
     */
  template<prop_test::concepts::Generator... Generator>
  auto one_of(const Generator & ...generator);

  namespace impl::one_of {
    template <typename T, typename... List>
    struct IsContained;

    template <typename T, typename Head, typename... Tail>
    struct IsContained<T, Head, Tail...>
    {
      static constexpr bool value = std::is_same_v<T, Head> || IsContained<T, Tail...>::value;
    };

    template <typename T>
    struct IsContained<T>
    {
      static constexpr bool value = false;
    };

    template<bool V, typename T, typename ...R>
    struct VariantOf;

    template<typename T, typename ...R>
    struct VariantOf<true, T, R...> {
      using type = std::variant<R...>;
    };

    template<typename T, typename ...R>
    struct VariantOf<false, T, R...> {
      using type = std::variant<T, R...>;
    };

    template<typename... T>
    struct VariantOr;

    template<typename T>
    struct VariantOr<T>{
      using type = T;
    };

    template<typename T>
    struct VariantOr<T, T>{
      using type = T;
    };

    template<typename T, typename... R>
    struct VariantOr<T, std::variant<R...>> {
      using type = typename VariantOf<IsContained<T, R...>::value, T, R...>::type;
    };

    template<typename T, typename R>
    struct VariantOr<T, R>{
      using type = std::variant<T, R>;
    };

    template<typename... Gen>
    struct OneOfCreateType;

    template<>
    struct OneOfCreateType<> {
      using type = void;
    };

    template<typename Gen>
    struct OneOfCreateType<Gen> {
      Gen g;
      using type = decltype(g.create({}));
    };

    template<typename Gen, typename... Gens>
    struct OneOfCreateType<Gen, Gens...> {
      Gen g;
      using my_type = decltype(g.create({}));
      using next_type = typename OneOfCreateType<Gens...>::type;
      using type = typename VariantOr<my_type, next_type>::type;
    };

    template<typename Result>
    struct Simplifier {
      [[nodiscard]] auto branches(const Result &v) const -> std::vector<Simplification<Result, Simplifier<Result>>> {
        return {};
      }

      [[nodiscard]] auto prefer(const Result &l, const Result &r) const -> Result { return r; }
    };

    template<typename ...Generators>
    struct Generator {
      using Result = typename OneOfCreateType<Generators...>::type;
      using Tuple = std::tuple<Generators...>;
      Tuple generators;

      /**
       * Creates a value from a seed
       * @param index Seed
       * @return
       */
      [[nodiscard]] auto create(size_t seed) const -> Result {
        auto rnd = std::mt19937_64(seed);
        const auto tupleIndex = rnd() % std::tuple_size_v<Tuple>;
        return create_impl(tupleIndex, rnd(), std::make_index_sequence<std::tuple_size_v<Tuple>>{});
      }

      /**
       * Creates a simplifier for a value
       * @param failedValue Value to simplify
       * @return
       */
      [[nodiscard]] auto simplifier(const Result& failedValue) const -> Simplification<Result, Simplifier<Result>> {
        return {
          .value = failedValue,
          .next = {Simplifier<Result>{}}};
      }

    private:

      template<size_t... Index>
      auto create_impl(size_t index, size_t seed, std::index_sequence<Index...>) const -> Result {
        auto vec = std::vector<std::optional<Result>>{};
        vec.resize(std::tuple_size_v<Tuple>);
        const auto _ = (create_impl_insert<Index>(vec, index, seed) && ...);
        return *vec[index];
      }

      template<size_t Index>
      auto create_impl_insert(std::vector<std::optional<Result>>& vec, size_t index, size_t seed) const -> bool {
        if (index != Index) {
          vec[index] = std::nullopt;
        }
        else {
          vec[index] = Result{std::get<Index>(generators).create(seed)};
        }
        return true;
      }
    };
  }

  template<prop_test::concepts::Generator... Generator>
  auto one_of(const Generator & ...generator) {
    return impl::one_of::Generator<Generator...>{.generators = std::make_tuple(generator...)};
  }
}
