#pragma once

#include <boost/pfr.hpp>
#include <prop_test/core.h>
#include <random>
#include <tuple>

namespace prop_test::gen::utils {

  /**
   * Combines multiple generators to create a ints of a resulting type
   * The provided inputs may be a generator
   * @tparam Result Type to wrap the result in. Results of generators will be passed in via an initializer list
   * @tparam Generators Types of generators to use
   * @param generatorMethods Generators (or callables which return generators) to combine
   * @return
   */
  template<typename Result, typename... Generators>
  auto combine(const Generators &...generators);

  namespace impl::combine {
    template<typename Gen>
    struct GenSimplifierType;

    template<concepts::ValueSimplifier Gen>
    struct GenSimplifierType<Gen>{
      Gen g;
      using type = decltype(g.simplifier(g.create({})));
    };

    template<concepts::IndexSimplifier Gen>
    struct GenSimplifierType<Gen>{
      Gen g;
      using type = decltype(g.simplify_index(size_t{}));
    };

    template<typename Result, typename... Generators>
    struct Simplifier {
      using Tuple = std::tuple<typename GenSimplifierType<Generators>::type...>;
      Tuple simplifiers;

      /**
     * Gets the simlification branches
     * @param v Value to simplify
     * @return
     */
      [[nodiscard]] auto branches(const Result &v) const -> std::vector<Simplification<Result, Simplifier<Result, Generators...>>> {
        auto res = std::vector<Simplification<Result, Simplifier<Result, Generators...>>>{};
        add_branches(v, res, std::make_index_sequence<sizeof...(Generators)>{});
        return res;
      }

      /**
     * Returns which value to prefer
     * @param l First option
     * @param r Second option
     * @return
     */
      [[nodiscard]] auto prefer(const Result &l, const Result &r) const -> Result {
        return r;
      }

    private:
      template<size_t... Index>
      auto add_branches(const Result &v, std::vector<Simplification<Result, Simplifier<Result, Generators...>>> &res, std::index_sequence<Index...>) const {
        const auto _ = (add_branch<Index>(v, res) | ...);
      }

      template<size_t Index>
      auto add_branch(const Result &v, std::vector<Simplification<Result, Simplifier<Result, Generators...>>> &res) const {
        const auto &simplifier = std::get<Index>(simplifiers);
        const auto &value = value_at<Index, Result>(v);
        auto branches = simplifier.branches();
        for (const auto &branch : branches) {
          auto copy = v;
          value_at<Index>(copy) = branch.value;
          auto newSimplifiers = simplifiers;
          std::get<Index>(newSimplifiers) = branch;
          res.emplace_back(Simplification<Result, Simplifier<Result, Generators...>>{
            .value = copy,
            .next = {newSimplifiers}});
        }
        return true;
      }

      template<size_t Index, typename T>
      static auto value_at(T& container) -> auto& {
        if constexpr (concepts::is_tuple_like<T>) {
          return std::get<Index>(container);
        }
        else {
          return boost::pfr::get<Index>(container);
        }
      }

      template<size_t Index, typename T>
      static auto value_at(const T& container) -> const auto& {
        if constexpr (concepts::is_tuple_like<T>) {
          return std::get<Index>(container);
        }
        else {
          return boost::pfr::get<Index>(container);
        }
      }
    };

    template<typename Result, typename... Generators>
    struct Generator {
      using type = Result;
      using Tuple = std::tuple<Generators...>;
      Tuple generators;

      /**
       * Creates a value from a seed
       * @param index Seed
       * @return
       */
      [[nodiscard]] auto create(size_t index) const -> Result {
        return create_impl(index, std::make_index_sequence<std::tuple_size<Tuple>::value>{});
      }

      /**
       * Creates a simplifier for a value
       * @param failedValue Value to simplify
       * @return
       */
      [[nodiscard]] auto simplify_index(const size_t index) const -> Simplification<Result, Simplifier<Result, Generators...>> {
        const auto r = create(index);
        return {
          .value = r,
          .next = {simplifier_impl(r, index, std::make_index_sequence<std::tuple_size<Tuple>::value>{})}};
      }

      Generator() : generators(std::make_tuple((Generators{})...)) {}
      Generator(const Tuple &tuple) : generators(tuple) {}

    private:
      template<size_t... Index>
      auto simplifier_impl(const Result& failed, const size_t index, std::index_sequence<Index...>) const {
        auto rnd = std::mt19937_64(index);
        return std::make_tuple((get_simplifier<Index>(rnd(), failed, std::get<Index>(generators)))...);
      }

      template<size_t Index, typename Gen>
      auto get_simplifier(size_t seed, const Result& failedItem, const Gen& generator) const {
        if constexpr (concepts::is_a_value_simplifier<Gen>) {
          if constexpr (concepts::is_tuple_like<Result>) {
            return generator.simplifier(std::get<Index>(failedItem));
          }
          else {
            return generator.simplifier(boost::pfr::get<Index>(failedItem));
          }
        }
        else {
          return generator.simplify_index(seed);
        }
      }

      template<size_t... Index>
      auto create_impl(size_t index, std::index_sequence<Index...>) const -> Result {
        auto rnd = std::mt19937_64(index);
        return Result{
          (std::get<Index>(generators).create(rnd()))...};
      }
    };

    template<typename Result, concepts::Generator... Generators>
    auto make_generator(const std::tuple<Generators...> &generators) -> Generator<Result, Generators...> {
      return Generator<Result, Generators...>{generators};
    }
  }// namespace impl::combine

  template<typename Result, typename... Generators>
  auto combine(const Generators &...generators) {
    auto tuple = std::make_tuple(generators...);
    return impl::combine::make_generator<Result>(tuple);
  }
}// namespace prop_test::gen::utils
