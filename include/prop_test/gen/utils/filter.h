#pragma once

#include <prop_test/core.h>
#include <functional>
#include <algorithm>

namespace prop_test::gen::utils {
    /**
     * Filters the output of a generator
     * > **Warning!** Too restrictive of a filter may make value generation and case shrinking increase in duration or take infinite time!
     * @tparam Generator Base generator type
     * @param filter Value filter
     * @param generator Base generator
     * @return
     */
    template<prop_test::concepts::Generator Generator>
    auto filter(
      const std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& filter,
      const Generator& generator
    );

    namespace impl::filter {
      template<typename Result, typename BaseSimplifier>
      struct Simplifier {
        std::function<bool (const Result&)> filter;
        BaseSimplifier baseSimplifier;

        [[nodiscard]] auto prefer(const Result& l, const Result& r) const -> Result { return r; }

        [[nodiscard]] auto branches(const Result& toSimplify) const -> std::vector<Simplification<Result, Simplifier>> {
          auto lastVals = std::vector<Result>{toSimplify};
          auto trials = 0;

          while (trials++ < 200 && !lastVals.empty()) {
            auto val = lastVals.back();
            lastVals.pop_back();

            auto simplifications = baseSimplifier.branches(val);
            for (const auto& s : simplifications) {
              lastVals.push_back(s.value);
            }
            simplifications.erase(
              std::remove_if(simplifications.begin(), simplifications.end(), [this](const auto &v) {
                return !filter(v.value);
              }),
              simplifications.end());
            if (!simplifications.empty()) {
              auto res = std::vector<Simplification<Result, Simplifier>>{};
              res.reserve(simplifications.size());

              std::transform(simplifications.begin(), simplifications.end(), std::back_inserter(res), [this](const auto& oldSimplification) {
                return Simplification<Result, Simplifier>{
                  .value = oldSimplification.value,
                  .next = Simplifier{
                    .filter = filter,
                    .baseSimplifier = oldSimplification.next
                  }
                };
              });

              return res;
            }
          }

          return {};
        }
      };

      template<typename BaseGenerator>
      struct Generator {
        using Result = decltype(std::declval<BaseGenerator>().create(size_t{}));
        using BaseSimplifier = decltype(std::declval<BaseGenerator>().simplifier(std::declval<Result>()).next);
        std::function<bool (const Result&)> filter;
        BaseGenerator generator;

        // Creates a value based on a random number
        [[nodiscard]] auto create(size_t index) const -> Result {
          auto res = generator.create(index);
          while (!filter(res)) {
            res = generator.create(++index);
          }
          return res;
        }

        // Creates a simplifier that starts at a specific value
        [[nodiscard]] auto simplifier(const Result& failed) const -> Simplification<Result, Simplifier<Result, BaseSimplifier>> {
          return {
            .value = failed,
            .next = Simplifier<Result, BaseSimplifier>{
              .baseSimplifier = generator.simplifier(failed).next,
              .filter = filter
            }
          };
        }
      };
    }

    template<prop_test::concepts::Generator Generator>
    auto filter(
      const std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& filter,
      const Generator& generator
    ) {
      return impl::filter::Generator<Generator>{
        .generator=generator,
        .filter=filter
      };
    }
}

