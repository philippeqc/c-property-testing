#pragma once

#include <prop_test/core.h>
#include <functional>
#include <algorithm>

namespace prop_test::gen::utils {
    /**
     * Maps (or transforms) the output of a ints.
     *
     * **Does not shrink output!**
     *
     * @tparam T resulting type
     * @tparam Generator Base generator type
     * @param mapper Value mapper
     * @param generator Base generator
     * @return
     */
    template<typename T, prop_test::concepts::Generator Generator>
    auto map(
      const std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& mapper,
      const Generator& generator
    );

    namespace impl::map {
      template<typename Result>
      struct Simplifier {
        [[nodiscard]] auto prefer(const Result& l, const Result& r) const -> Result { return r; }
        [[nodiscard]] auto branches(const Result& toSimplify) const -> std::vector<Simplification<Result, Simplifier>> { return {}; }
      };

      template<typename Result, typename BaseGenerator>
      struct Generator {
        using GenResult = decltype(std::declval<BaseGenerator>().create(size_t{}));
        using BaseSimplifier = decltype(std::declval<BaseGenerator>().simplifier(std::declval<Result>()).next);
        std::function<Result (const GenResult&)> mapper;
        BaseGenerator generator;

        // Creates a value based on a random number
        [[nodiscard]] auto create(size_t index) const -> Result {
          return mapper(generator.create(index));
        }

        // Creates a simplifier that starts at a specific value
        [[nodiscard]] auto simplifier(const Result& failed) const -> Simplification<Result, Simplifier<Result>> {
          return {
            .value = failed,
            .next = Simplifier<Result>{}
          };
        }
      };
    }

    template<typename T, prop_test::concepts::Generator Generator>
    auto map(
      const std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& mapper,
      const Generator& generator
    ) {
      return impl::map::Generator<T, Generator>{
        .mapper=mapper,
        .generator=generator
      };
    }
}

