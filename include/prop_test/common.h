#pragma once

namespace prop_test {
  /**
   * Data version to use for data sets
   */
  enum class DataVersion {
    VERSION_0,

    LATEST = VERSION_0
  };
}

namespace prop_test::impl {
  template<typename Gen>
  struct GenCreationType {
    Gen g;
    using type = decltype(g.create({}));
  };
}
